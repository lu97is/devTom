const mongoose = require('mongoose')
mongoose.Promise = global.Promise;
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt-nodejs');

const adminSchema = new Schema({
  username: {type: String, required: true, unique: true},
  password: {type: String, required: true, unique: true},
  city: {type: String, lowercase: true},
  sede: {type: String},
  role: {type: String, default: 'admin'}
})

adminSchema.pre('save', function(next){
  if (!this.isModified('password')) {
    return next();
  }
  else{
    bcrypt.hash(this.password, null, null,(err, hash)=>{
      if (err) return next(err);
      else{
        this.password = hash;
        next();
      }
    })
  }
});

adminSchema.methods.comparePass = function(password){
  return bcrypt.compareSync(password, this.password);
}

module.exports = mongoose.model('Admin',adminSchema);
