const mongoose = require('mongoose')
mongoose.Promise = global.Promise;
const Schema = mongoose.Schema;
const titlize = require('mongoose-title-case');

const eventSchema = new Schema({
  name : {type: String, required: true},
  //mover
  city: {type: String, required: true},
  sede: {type: String, required: true},
  type: {type:String, required: true},
  forWho: {type:String, required: true},
  myDate: {type:String, required: true},
  myDateEnd: {type:String, required: true},
  price: {type:Number},
  description: {type:String, required: true},
  adress: {
    country: {type: String, default: 'Mexico', required: true},
    street: {type: String, required: true},
    number: {type: Number}
  },
  email: {type: String},
  phone: {type: String}
})

eventSchema.plugin(titlize, {
  paths: [ 'name']
});


module.exports = mongoose.model('Event',eventSchema);
