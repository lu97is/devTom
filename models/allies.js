const mongoose = require('mongoose')
mongoose.Promise = global.Promise;
const Schema = mongoose.Schema;
const titlize = require('mongoose-title-case');
// const validators = require('mongoose-validators');

const alliesSchema = new Schema({
  name : {type: String, lowercase:true, unique: true},
  address: {type: String, required: true},
  imgUrl: {type: String, required: true}
})

alliesSchema.plugin(titlize, {
  paths: [ 'name']
});


module.exports = mongoose.model('Ally',alliesSchema);
