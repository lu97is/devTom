const mongoose = require('mongoose')
mongoose.Promise = global.Promise;
const Schema = mongoose.Schema;
const titlize = require('mongoose-title-case');

const clientSchema = new Schema({
  name : {type: String, required: true},
  logo: {type: String},
  webPage: {type: String},
  desc: {type: String},
  company: {type: String, required: true}
})

clientSchema.plugin(titlize, {
  paths: [ 'name']
});


module.exports = mongoose.model('Client',clientSchema);
