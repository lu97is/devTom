const mongoose = require('mongoose')
mongoose.Promise = global.Promise;
const Schema = mongoose.Schema;
const titlize = require('mongoose-title-case');
// const validators = require('mongoose-validators');

const citySchema = new Schema({
  name : {type: String, lowercase:true, unique: true}
})

citySchema.plugin(titlize, {
  paths: [ 'name']
});


module.exports = mongoose.model('City',citySchema);
