const mongoose = require('mongoose')
mongoose.Promise = global.Promise;
const Schema = mongoose.Schema;

const updateSchema = new Schema({
  info : {type: String, lowercase: true},
  date: {type: String},
  username: {type: String},
  city: {type: String},
  sede: {type: String},
  priority: {type: Number}
})


module.exports = mongoose.model('Update',updateSchema);
