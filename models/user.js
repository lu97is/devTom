const mongoose = require('mongoose')
mongoose.Promise = global.Promise;
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt-nodejs');
const titlize = require('mongoose-title-case');

const userSchema = new Schema({
  email: {type: String, required: true, unique: true},
  password: {type: String},
  name : {
    firstName: {type: String, default: ''},
    lastName: {type: String}
  },
  habilities: [{type: String, default: ''}],
  interest: [{type: String, default: ''}],
  status: {
    work: {type: String},
    study: {type: String}
  },
  role: {type: String, default: 'user'},
  socialId: {type: String},
  imageUrl: {type:String, default: 'default'},
  backUrl: {type:String, default: 'default'},
  social: {
    facebook: {type: String, default: ''},
    twitter: {type: String},
    instagram: {type: String}
  },
  // phone: {type: Number},
  address: {type: String},
  profession: {type: String},
  bio: {type: String}
});

userSchema.pre('save', function(next){
  if (!this.isModified('password')) {
    return next();
  }
  else{
    bcrypt.hash(this.password, null, null,(err, hash)=>{
      if (err) return next(err);
      else{
        this.password = hash;
        next();
      }
    })
  }
});

userSchema.methods.comparePass = function(password){
  return bcrypt.compareSync(password, this.password);
}

userSchema.plugin(titlize, {
  paths: [ 'name.firstName', 'name.lastName', 'status.place']
});

module.exports = mongoose.model('User',userSchema)
