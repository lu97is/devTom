const mongoose = require('mongoose')
mongoose.Promise = global.Promise;
const Schema = mongoose.Schema;
const titlize = require('mongoose-title-case');
// const validators = require('mongoose-validators');
const companySchema = new Schema({
  name : {type: String, unique: true, required: true, lowercase: true},
  description: {type: String, required: true},
  website: {type: String},
  phrase: {type: String},
  approach: {type: String},
  services: [{type: String}],
  adress: {
    country: {type: String, default: 'Mexico', required: true},
    state: {type: String, required: true},
    street: {type: String, required: true},
    number: {type: Number}
  },
  number: {
    cellphone: {type: Number},
    phone: {type: Number, required: true}
  },
  email: {type: String, required: true},
  user: {type: String, required: true},
  userId: {type: String, required: true},
  tecnologies: [{type: String}],
  cluster: {type: Boolean},
  rfc: {type: String},
  imageUrl: {type: String, default: 'default'},
  backUrl: {type: String, default: 'default'}
})
companySchema.plugin(titlize, {
  paths: [ 'name', 'services', 'tecnologies']
});
module.exports = mongoose.model('Company',companySchema);
