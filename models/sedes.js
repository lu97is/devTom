const mongoose = require('mongoose')
mongoose.Promise = global.Promise;
const Schema = mongoose.Schema;
const titlize = require('mongoose-title-case');

const sedeSchema = new Schema({
  name : {type: String, lowercase: true, required: true},
  city: {type: String, lowercase:true, required: true}
})

sedeSchema.plugin(titlize, {
  paths: [ 'name']
});


module.exports = mongoose.model('Sedes',sedeSchema);
