const mongoose = require('mongoose')
mongoose.Promise = global.Promise;
const Schema = mongoose.Schema;
const titlize = require('mongoose-title-case');

const workSchema = new Schema({
  name : {type: String, required: true},
  logo: {type: String},
  desc: {type: String},
  company: {type: String, required: true},
  type: {type: String}
})

workSchema.plugin(titlize, {
  paths: [ 'name']
});


module.exports = mongoose.model('Work',workSchema);
