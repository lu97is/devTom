const mongoose = require('mongoose')
mongoose.Promise = global.Promise;
const Schema = mongoose.Schema;
const titlize = require('mongoose-title-case');

const teamSchema = new Schema({
  name : {type: String, required: true},
  img: {type: String},
  position: {type: String},
  company: {type: String, required: true},
})

teamSchema.plugin(titlize, {
  paths: [ 'name']
});


module.exports = mongoose.model('Team',teamSchema);
