const mongoose = require('mongoose')
mongoose.Promise = global.Promise;
const Schema = mongoose.Schema;
const titlize = require('mongoose-title-case');
// const validators = require('mongoose-validators');

const contactSchema = new Schema({
  name : {type: String, required: true},
  email: {type: String, required: true},
  bussines: {type: String, required: true},
  sede: {type: String, required: true},
  text: {type: String, required: true}
})



module.exports = mongoose.model('Contact',contactSchema);
