const User = require('../models/user');
const Sede = require('../models/sedes');
const Admin = require('../models/admin');
const Event = require('../models/evento');
const Company = require('../models/empresa');
const City = require('../models/ciudad');
const Update = require('../models/update');
const Startup = require('../models/startup');
const Ally = require('../models/allies');
const Contact = require('../models/contact');
const Client = require('../models/client');
const Works = require('../models/works');
const Team = require('../models/team');
const jwt = require('jsonwebtoken');
const config = require('../config/database');
const multer = require('multer');
const multerUp = multer();
const Crypto = require('crypto');
const xoauth2 = require('xoauth2');
const nodemailer = require('nodemailer');
module.exports = (router) => {

  router.post('/register', (req, res) => {
    if (!req.body.email) {
      res.json({
        succes: false,
        message: 'no email '
      })
    } else {
      let user = new User();
      let habilities = [];
      let interest = [];
      user.email = req.body.email;
      user.password = req.body.password;
      user.name.firstName = req.body.firstName;
      user.name.lastName = req.body.lastName;
      user.status.position = req.body.position;
      user.status.study = req.body.study;
      user.status.work = req.body.work;
      user.habilities = req.body.habilities;
      user.interest = req.body.interest;
      user.imageUrl = req.body.imageUrl;
      user.save((err) => {
        if (err) {
          if (err.code == 11000) {
            res.json({
              succes: false,
              message: 'Email ya registrado'
            })
          } else {
            res.json({
              succes: false,
              message: err
            })
          }
        } else {
          res.json({
            succes: true,
            message: "User saved"
          })
        }
      });
    }
  });

  router.post('/register/social', (req, res) => {
    if (!req.body.email) {
      res.json({
        succes: false,
        message: 'no email'
      })
    } else {
      let user = new User()
        user.email= req.body.email
        user.socialId= req.body.socialId
        user.imageUrl= req.body.imageUrl
        user.name.firstName = req.body.name
      user.save((err) => {
        if (err) {
          if (err.code === 11000) {
            res.json({
              succes: false,
              err: 'email register'
            })
          } else {
            res.json({
              succes: false,
              err: err
            })
          }
        } else {
          res.json({
            succes: true,
            message: 'Guardado'
          })
        }
      })
    }
  })

  router.post('/registerSede', (req, res) =>  {
    if (!req.body.name) {
      res.json({
        succes: false,
        message: 'Name not provided'
      })
    } else if (!req.body.city) {
      res.json({
        succes: false,
        message: 'City not provided'
      })
    } else {
      let sede = new Sede({
        name: req.body.name,
        city: req.body.city
      });
      sede.save((err) => {
        if (err) {
          res.json({
            succes: false,
            message: 'Ops algo va mal'
          })
        } else
        if (err === 11000) {
          res.json({
            succes: false,
            message: 'Ciudad ya registrada'
          })
        } else {
          res.json({
            succes: true,
            message: 'Exito'
          })
        }
      })
    }
  })

  router.post('/registerEvent', (req, res) => {
    if (!req.body.name) {
      res.json({
        succes: false,
        message: 'Name not provided'
      })
    } else if (!req.body.sede) {
      res.json({
        succes: false,
        message: 'Sede not provided'
      })
    } else if (!req.body.city) {
      res.json({
        succes: false,
        message: 'City not provided'
      })
    } else {
      let event = new Event();
      event.name = req.body.name;
      event.city = req.body.city;
      event.sede = req.body.sede;
      event.type = req.body.type;
      event.forWho = req.body.forWho;
      event.myDate = req.body.myDate;
      event.myDateEnd = req.body.myDateEnd;
      event.price = req.body.price;
      event.description = req.body.description;
      event.adress.street = req.body.street;
      event.adress.number = req.body.number;
      event.email = req.body.email;
      event.phone = req.body.phone;
      event.save((err) => {
        if (err) {
          res.json({
            succes: false,
            message: err
          })
        } else {
          res.json({
            succes: true,
            message: 'Exito'
          })
        }
      })
    }
  })

  router.get('/employes', (req, res) => {
    User.find({}).exec((err, user) => {
      if (err) {
        res.json({
          success: false,
          message: err
        })
      } else {
        res.json({
          success: true,
          message: user
        })
      }
    })
  })
  router.post('/registerUpdate', (req, res) => {
    if (!req.body.info) {
      res.json({
        success: false,
        message: 'complete all fields'
      })
    } else {
      let update = new Update({
        info: req.body.info,
        date: req.body.date,
        username: req.body.username,
        city: req.body.city,
        sede: req.body.sede,
        priority: req.body.priority
      });
      update.save((err) => {
        if (err) {
          res.json({
            success: false,
            message: err
          })
        } else {
          res.json({
            success: true,
            message: 'Exito'
          })
        }
      })
    }
  })

  router.post('/registerCity', (req, res) => {
    if (!req.body.name) {
      res.json({
        succes: false,
        message: 'Name not provided'
      })
    } else {
      let city = new City({
        name: req.body.name
      });
      city.save((err) => {
        if (err) {
          if (err.code === 11000) {
            res.json({
              succes: false,
              message: err
            })
          } else {
            res.json({
              succes: false,
              message: err
            })
          }
        } else {
          res.json({
            succes: true,
            message: 'Exito'
          })
        }
      })
    }
  })


  router.post('/adminReg', (req, res) => {
    if (!req.body.username) {
      res.json({
        succes: false,
        message: 'username not provided'
      })
    } else if (!req.body.password) {
      res.json({
        succes: false,
        message: 'password not provided'
      })
    } else if (req.body.secret != 19971710) {
      res.json({
        succes: false,
        message: 'you shouldnt be here...'
      })
    } else {
      let admin = new Admin({
        username: req.body.username,
        password: req.body.password,
        city: req.body.city,
        sede: req.body.sede
      })
      admin.save((err) => {
        if (err) {
          if (err.code === 11000) {
            res.json({
              succes: false,
              message: 'Nombre de usuario no disponible'
            })
          } else {
            res.json({
              succes: false,
              message: err
            })
          }
        } else {
          res.json({
            succes: true,
            message: 'Admin Guardado'
          })
        }
      })
    }
  })

  router.get('/emails', (req, res) => {
    User.find({}).exec((err, user) => {
      if (err) {
        res.json({
          success: false,
          message: err
        })
      } else {
        res.send(user)
      }
    })
  })

  router.get('/admins', (req, res) => {
    Admin.find({}).exec((err, admin) => {
      if (err) {
        res.json({
          succes: false,
          message: err
        });
      } else {
        res.send(admin);
      }
    })
  })

  router.get('/events', (req, res) => {
    Event.find({}).exec((err, eve) => {
      if (err) {
        res.json({
          success: false,
          message: err
        })
      } else {
        res.send(eve);
      }
    })
  })



  router.get('/loadSedes', (req, res) => {
    Sede.find({}).exec((err, sedes) => {
      if (err) {
        res.json({
          succes: false,
          message: err
        })
      } else {
        res.send(sedes)
      }
    })
  })

  router.get('/loadCities', (req, res) => {
    City.find({}).exec((err, cities) => {
      if (err) {
        res.json({
          succes: false,
          message: err
        })
      } else {
        res.send(cities)
      }
    })
  })



  router.post('/login', (req, res) => {
    if (!req.body.email) {
      res.json({
        succes: false,
        message: 'No email provided'
      })
    } else if (!req.body.password) {
      res.json({
        succes: false,
        message: 'No password provided'
      })
    } else {
      User.findOne({
        email: req.body.email
      }, (err, user) => {
        if (err) {
          res.json({
            succes: false,
            message: 'Error' + err
          })
        } else {
          if (!user) {
            res.json({
              succes: false,
              message: 'No user found'
            })
          } else if (user.socialId) {
            res.json({
              succes: false,
              message: 'Usa la red social registrada'
            })
          } else {
            const validPassword = user.comparePass(req.body.password);
            if (!validPassword) {
              res.json({
                succes: false,
                message: 'Wrong password'
              })
            } else {
              const token = jwt.sign({
                userId: user._id,
                email: user.email
              }, config.secret, {
                expiresIn: '7d'
              });
              res.json({
                succes: true,
                message: "Authentucated",
                token: token,
                user: {
                  email: user.email
                }
              })
            }
          }
        }
      })
    }
  })

  router.post('/socialLogin', (req, res) => {
    User.findOne({
      email: req.body.email
    }, (err, user) => {
      if (err) {
        res.json({
          succes: false,
          message: err
        })
      } else {
        const token = jwt.sign({
          userId: user._id,
          email: user.email
        }, config.secret, {
          expiresIn: '7d'
        });
        res.json({
          succes: true,
          message: 'Authentucated',
          token: token,
          user: {
            email: user.email
          }
        })
      }
    })
  })


  router.post('/adminLog', (req, res) => {
    if (!req.body.username) {
      res.json({
        succes: false,
        message: 'username not provided'
      })
    } else if (!req.body.password) {
      res.json({
        succes: false,
        message: 'password not provided'
      })
    } else if (!req.body.secret) {
      res.json({
        succes: false,
        message: 'secret not provided'
      })
    } else {
      if (req.body.secret != 19971710) {
        res.json({
          succes: false,
          message: 'you shouldnt be here'
        })
      } else {
        Admin.findOne({
          username: req.body.username
        }, (err, admin) => {
          if (err) {
            res.json({
              succes: false,
              message: err
            })
          } else {
            if (!admin) {
              res.json({
                succes: false,
                message: 'Admin not found'
              })
            } else {
              let validPassword = admin.comparePass(req.body.password);
              if (!validPassword) {
                res.json({
                  succes: false,
                  message: 'Wrong password'
                })
              } else {
                const token = jwt.sign({
                  adminId: admin._id,
                  role: admin.role
                }, config.secret, {
                  expiresIn: '7d'
                });
                res.json({
                  succes: true,
                  message: 'Authenticated',
                  token: token,
                  admin: admin
                })
              }
            }
          }
        })
      }
    }
  })

  router.post('/forgot', (req, res) => {
    Crypto.randomBytes(20)
  })

  router.put('/admin/event/edit/:id', (req, res) => {
    Event.findByIdAndUpdate(req.params.id, {
      $set: req.body
    }, {
      new: true
    }, (err, eve) => {
      if (err) {
        res.json({
          success: false,
          message: err
        })
      } else {
        res.json({
          success: true,
          message: eve
        })
      }
    })
  })

  router.delete('/event/:id', (req, res) => {
    Event.remove({
      _id: req.params.id
    }, (err) => {
      if (err) {
        res.json({
          success: false,
          message: err
        })
      } else {
        res.json({
          success: true,
          message: 'Deleted'
        })
      }
    })
  })

  router.get('/event/:id', (req, res) => {
    Event.findOne({
      _id: req.params.id
    }, (err, even) => {
      if (err) {
        res.json({
          success: false,
          message: err
        })
      } else {
        res.json({
          success: true,
          message: even
        })
      }
    })
  })

  router.get('/admin/event/edit/:id', (req, res) => {
    Event.findOne({
      _id: req.params.id
    }, (err, eve) => {
      if (err) {
        res.json({
          success: false,
          message: err
        })
      } else {
        res.json({
          success: true,
          message: eve
        })
      }
    })
  })

  router.delete('/admin/:id', (req, res) => {
    Admin.remove({
      _id: req.params.id
    }, (err) => {
      if (err) {
        res.json({
          success: false,
          message: err
        })
      } else {
        res.json({
          success: true,
          message: 'Deleted'
        })
      }
    })
  })

  router.get('/updates', (req, res) => {
    Update.find({}).exec((err, updates) => {
      if (err) {
        res.json({
          succes: false,
          message: err
        })
      } else {
        res.send(updates)
      }
    })
  })


  router.post('/registerCompany', (req, res) => {
    if (!req.body.name) {
      res.json({
        succes: false,
        message: 'name not provided'
      })
    } else if (!req.body.description) {
      res.json({
        succes: false,
        message: 'description not provided'
      })
    } else if (!req.body.phone) {
      res.json({
        succes: false,
        message: 'phone not provided'
      })
    } else if (!req.body.email) {
      res.json({
        succes: false,
        message: 'email not provided'
      })
    } else if (!req.body.user) {
      res.json({
        succes: false,
        message: 'user not provided'
      })
    } else {
      let company = new Company(req.body);
      company.adress.state = req.body.state;
      company.adress.street = req.body.street;
      company.adress.number = req.body.number;
      company.number.cellphone = req.body.cellphone;
      company.number.phone = req.body.phone;
      company.save((err) => {
        if (err) {
          if (err.code == 11000) {
            res.json({
              succes: false,
              message: 'company already registered'
            })
          } else {
            res.json({
              succes: false,
              err: err
            })
          }
        } else {
          res.json({
            succes: true,
            message: 'Exito'
          })
        }
      })
    }
  })

  router.post('/registerStartup', (req, res) => {
    if (!req.body.name) {
      res.json({
        succes: false,
        message: 'name not provided'
      })
    } else if (!req.body.description) {
      res.json({
        succes: false,
        message: 'description not provided'
      })
    } else if (!req.body.phone) {
      res.json({
        succes: false,
        message: 'phone not provided'
      })
    } else if (!req.body.email) {
      res.json({
        succes: false,
        message: 'email not provided'
      })
    } else if (!req.body.user) {
      res.json({
        succes: false,
        message: 'user not provided'
      })
    } else {
      let startup = new Startup(req.body);
      startup.adress.state = req.body.state;
      startup.adress.street = req.body.street;
      startup.adress.number = req.body.number;
      startup.number.cellphone = req.body.cellphone;
      startup.number.phone = req.body.phone;
      startup.save((err) => {
        if (err) {
          if (err.code == 11000) {
            res.json({
              succes: false,
              message: 'startup already registered'
            })
          } else {
            res.json({
              succes: false,
              err: err
            })
          }
        } else {
          res.json({
            succes: true,
            message: 'Exito'
          })
        }
      })
    }
  })

  router.post('/registerAllie/photo', (req, res) => {
    let DIR = './public/assets/img/allies'
    let allie = new Ally();
    let random = Crypto.randomBytes(12).toString('hex');
    let storage = multer.diskStorage({
      destination: (req, file, cb) => {
        cb(null, DIR)
      },
      filename: (req, file, cb) => {
        cb(null, random + Date.now() + random + '.jpg')
      }
    })
    let upload = multer({
      storage: storage
    }).single('allieImg');
    upload(req, res, (err) => {
      if (err) {
        res.json({
          succes: false,
          message: err
        })
      } else {
        allie.name = req.body.name;
        allie.address = req.body.address;
        allie.imgUrl = req.file.filename;
        allie.save((err) => {
          if (err) {
            res.json({
              succes: false,
              message: err
            })
          } else {
            res.json({
              succes: true,
              message: 'Exito'
            })
          }
        })
      }
    })
  })

  router.get('/companies', (req, res) => {
    Company.find({}).exec((err, companies) => {
      if (err) {
        res.json({
          succes: false,
          err: err
        })
      } else {
        res.json({
          succes: true,
          companies: companies
        })
      }
    })
  })

  router.post('/contact', (req, res) => {
    let contact = new Contact();
    contact.name = req.body.name;
    contact.email = req.body.email;
    contact.bussines = req.body.bussines;
    contact.sede = req.body.sede;
    contact.text = req.body.text;
    contact.save((err) => {
      if (err) {
        res.json({
          succes: false,
          message: err
        })
      } else {
        res.json({
          succes: true,
          message: 'Guardado'
        })
      }
    })
  })


  router.get('/allies', (req, res) => {
    Ally.find({}).sort({
      $natural: -1
    }).limit(2).exec((err, all) => {
      if (err) {
        res.json({
          succes: false,
          message: err
        })
      } else {
        res.json({
          succes: true,
          message: all
        })
      }
    })
  })

  router.post('/email', (req, res) => {
    let transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        xoauth2: xoauth2.createXOAuth2Generator({
          user: 'lu97is@gmail.com',
          clientId: '167231319931-khlkqgisnt5ctd62h54ef33bacrrre4u.apps.googleusercontent.com',
          clientSecret: '1HPX8u56znyuIvNTOlIqzW-h',
          refreshToken: '1/kbDSQMQvHUqIK7hrqJpEEHLaW6Z8y39shMp_cpybJrM'
        })
      }
    });
    let mailOptions = {
      from: 'Luis <lu97is@gmail.com>',
      to: 'jorgeguzman1196@gmail.com',
      subject: 'Probando enviar email por aqui',
      text: 'porvadnfuew rewqurenwqjr ejwrenjwre qnjr renj'
    };
    transporter.sendMail((err, send) => {
      if (err) {
        res.json({
          succes: false,
          message: err
        })
      } else {
        res.json({
          succes: true,
          message: send
        })
      }
    })
  })

  router.get('/companies/cluster', (req, res) => {
    Company.find({
      cluster: true
    }).exec((err, companies) => {
      if (err) {
        res.json({
          succes: false,
          message: err
        })
      } else {
        res.json({
          succes: true,
          message: companies
        })
      }
    })
  })

  router.get('/company/:name', (req, res) => {
    Company.findOne({
      name: req.params.name
    }).exec((err, company) => {
      if (err) {
        res.json({
          succes: false,
          err: err
        })
      } else {
        res.json({
          succes: true,
          company: company
        })
      }
    })
  })




  router.use((req, res, next) => {
    const token = req.headers['authorization'];
    if (!token) {
      res.json({
        succes: false,
        message: 'No token provided'
      })
    } else {
      jwt.verify(token, config.secret, (err, decoded) => {
        if (err) {
          res.json({
            succes: false,
            message: 'token invalid' + err
          })
        } else {
          req.decoded = decoded;
          next();
        }
      })
    }
  })

  router.get('/profile', (req, res) => {
    User.findOne({
      _id: req.decoded.userId
    }).select('_id').exec((err, user) => {
      if (err) {
        res.json({
          succes: false,
          message: err
        })
      } else
      if (!user) {
        res.json({
          succes: false,
          message: 'no user'
        })
      } else {
        res.json({
          succes: true,
          user: user
        })
      }
    })
  })

  router.get('/profileInfo', (req, res) => {
    User.findOne({
      _id: req.decoded.userId
    }).exec((err, user) => {
      if (err) {
        res.json({
          succes: false,
          err: err
        })
      } else {
        res.json({
          succes: true,
          user: user
        })
      }
    })
  })

  router.get('/profile/:email', (req,res)=>{
    User.findOne({email: req.params.email},(err,user)=>{
      if (err) {
        res.json({succes: false, message: err})
      } else {
        res.json({succes: true, message: user})
      }
    })
  })

  router.get('/companies/self', (req, res) => {
    Company.find({
      userId: req.decoded.userId
    }).exec((err, company) => {
      if (err) {
        res.json({
          succes: false,
          err: err
        })
      } else {
        res.json({
          succes: true,
          company: company
        })
      }
    })
  })

  
  router.get('/startups/self', (req, res) => {
    Startup.find({
      userId: req.decoded.userId
    }).exec((err, startup) => {
      if (err) {
        res.json({
          succes: false,
          err: err
        })
      } else {
        res.json({
          succes: true,
          startup: startup
        })
      }
    })
  })


  router.post('/works', (req, res) => {
    if (!req.body.company) {
      res.json({
        succes: false,
        message: 'Falta compañia'
      })
    }
    if (!req.body.name) {
      res.json({
        succes: false,
        message: 'Falta nombre'
      })
    } else {
      let work = new Works(req.body);
      work.save((err) => {
        if (err) {
          res.json({
            succes: false,
            message: err
          })
        } else {
          res.json({
            succes: true,
            message: 'Exito'
          })
        }
      })
    }
  });

  router.post('/team', (req, res) => {
    if (!req.body.company) {
      res.json({
        succes: false,
        message: 'Falta compañia'
      })
    }
    if (!req.body.name) {
      res.json({
        succes: false,
        message: 'Falta nombre'
      })
    } else {
      let team = new Team(req.body);
      team.save((err) => {
        if (err) {
          res.json({
            succes: false,
            message: err
          })
        } else {
          res.json({
            succes: true,
            message: 'Exito'
          })
        }
      })
    }
  });

  router.get('/clients/:company', (req, res) => {
    Client.find({
      company: req.params.company
    }, (err, cli) => {
      if (err) {
        res.json({
          succes: false,
          message: err
        })
      } else {
        res.json({
          succes: true,
          message: cli
        })
      }
    })
  })

  router.get('/works/:company', (req, res) => {
    Works.find({
      company: req.params.company
    }, (err, cli) => {
      if (err) {
        res.json({
          succes: false,
          message: err
        })
      } else {
        res.json({
          succes: true,
          message: cli
        })
      }
    })
  })

  router.get('/team/:company', (req, res) => {
    Team.find({
      company: req.params.company
    }, (err, cli) => {
      if (err) {
        res.json({
          succes: false,
          message: err
        })
      } else {
        res.json({
          succes: true,
          message: cli
        })
      }
    })
  })

  router.get('/client/last/:company', (req,res)=>{
    Client.find({company: req.params.company},{},{sort: {'.id': '-1'}},(err,cli)=>{
      if (err) {
        res.json({succes: false, message: err})
      } else {
        res.json({succes: true, message:cli})
      }
    })
  })

  router.put('/register/complete', (req, res) => {
    User.findByIdAndUpdate(req.decoded.userId, {
      $set: req.body
        // 'name.firstName' : req.body.name.firstName,
        // 'name.lastName' : req.body.name.lastName,
        // 'status.study' : req.body.study,
        // 'status.work': req.body.work,
        // 'habilities' : req.body.habilities,
        // 'interest' : req.body.interest,
        // 'phone': req.body.phone,
        // 'social.facebook': req.body.social.facebook
        }, {
      new: true
    }, (err, update) => {
      if (err) {
        res.json({
          succes: false,
          message: err
        })
      } else {
        res.json({
          succes: true,
          update: update,
          message: 'Campos actualizados'
        })
      }
    })
  })

  router.post('/clients', (req,res)=>{
  if (!req.body.company) {
      res.json({
        succes: false,
        message: 'Falta compañia'
      })
    }
    if (!req.body.name) {
      res.json({
        succes: false,
        message: 'Falta nombre de cliente'
      })
    } else {
      let client = new Client(req.body);
      client.save((err) => {
        if (err) {
          res.json({
            succes: false,
            message: err
          })
        } else {
          res.json({
            succes: true,
            message: 'Exito'
          })
        }
      })
    }
  })

//   router.post('/clients', multer.upload([]), (req, res) => {
//     let DIR = './public/assets/img/companies';
//     let storage = multer.diskStorage({
//       destination: (req, file, cb) => {
//         cb(null, DIR)
//       },
//       filename: (req, file, cb) => {
//         cb(null, req.params.client +  'client.jpg')
//       }
//     })
//     let upload = multer({
//       storage: storage
//     }).single('logoImage');
//     let path = '';
//     upload(req, res, (err) => {
//       if (err) {
//         res.json({
//           succes: false,
//           message: err
//         })
//       }else{
//         res.json({
//           succes: true,
//           message: 'Guardado'
//         })
//       }
//   })
// });
  

  router.post('/upload', (req, res, next) => {
    //dev
    let DIR = './public/assets/img/users';
    //prod
    // let DIR = './uploads/users/';
    let storage = multer.diskStorage({
      destination: (req, file, cb) => {
        cb(null, DIR)
      },
      filename: (req, file, cb) => {
        cb(null, req.decoded.userId + '.jpg')
      }
    })
    let upload = multer({
      storage: storage
    }).single('profileImg');
    let path = '';
    upload(req, res, (err) => {
      if (err) {
        res.json({
          succes: false,
          message: err
        })
      } else {
        // path = req.file.filename;
        User.findOneAndUpdate({
          _id: req.decoded.userId
        }, {
          $set: {
            imageUrl: req.file.filename
          }
        }, (err, doc) => {
          if (err) {
            res.json({
              succes: false,
              message: err
            })
          } else {
            res.json({
              succes: true,
              message: 'Actualizado'
            })
          }
        })
      }
    })
  });
  router.post('/uploadBack', (req, res, next) => {
    //dev
    let DIR = './public/assets/img/users';
    //prod
    // let DIR = './uploads/users/';
    let storage = multer.diskStorage({
      destination: (req, file, cb) => {
        cb(null, DIR)
      },
      filename: (req, file, cb) => {
        cb(null, req.decoded.userId + 'back.jpg')
      }
    })
    let upload = multer({
      storage: storage
    }).single('profileImg');
    let path = '';
    upload(req, res, (err) => {
      if (err) {
        res.json({
          succes: false,
          message: err
        })
      } else {
        // path = req.file.filename;
        User.findOneAndUpdate({
          _id: req.decoded.userId
        }, {
          $set: {
            backUrl: req.file.filename
          }
        }, (err, doc) => {
          if (err) {
            res.json({
              succes: false,
              message: err
            })
          } else {
            res.json({
              succes: true,
              message: 'Actualizado'
            })
          }
        })
      }
    })
  });
  router.post('/upload/company/:name', (req, res, next) => {
    //dev
    // let DIR = './client/src/assets/img/companies';
    //prod
    let DIR = './public/assets/img/companies'
    let storage = multer.diskStorage({
      destination: (req, file, cb) => {
        cb(null, DIR)
      },
      filename: (req, file, cb) => {
        cb(null, req.params.name + '.jpg')
      }
    })
    let upload = multer({
      storage: storage
    }).single('profileImg');
    let path = '';
    upload(req, res, (err) => {
      if (err) {
        res.json({
          succes: false,
          message: err
        })
      } else {
        // path = req.file.filename;
        Company.findOneAndUpdate({
          userId: req.decoded.userId,
          name: req.params.name
        }, {
          $set: {
            imageUrl: req.file.filename
          }
        }, (err, doc) => {
          if (err) {
            res.json({
              succes: false,
              message: err
            })
          } else {
            res.json({
              succes: true,
              message: 'Actualizado'
            })
          }
        })
      }
    })
  });
  router.post('/uploadBack/company/:name', (req, res, next) => {
    //dev
    // let DIR = './client/src/assets/img/companies';
    //prod
    let DIR = './public/assets/img/companies'
    let storage = multer.diskStorage({
      destination: (req, file, cb) => {
        cb(null, DIR)
      },
      filename: (req, file, cb) => {
        cb(null, req.params.name + 'back.jpg')
      }
    })
    let upload = multer({
      storage: storage
    }).single('profileImg');
    let path = '';
    upload(req, res, (err) => {
      if (err) {
        res.json({
          succes: false,
          message: err
        })
      } else {
        // path = req.file.filename;
        Company.findOneAndUpdate({
          userId: req.decoded.userId,
          name: req.params.name
        }, {
          $set: {
            backUrl: req.file.filename
          }
        }, (err, doc) => {
          if (err) {
            res.json({
              succes: false,
              message: err
            })
          } else {
            res.json({
              succes: true,
              message: 'Actualizado'
            })
          }
        })
      }
    })
  });
  return router;
}