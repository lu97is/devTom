import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services/user/user.service';
import { Router} from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { tokenNotExpired } from 'angular2-jwt';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  log: string;
  options: any [] = ['INICIO', 'EVENTOS', 'EMPRESAS', 'NOTICIAS'];
  optionsUrl: any[] = ['home', 'events', 'companies', 'news', 'closter'];
  loaded = false;
  profile: any[] = [];
  constructor(private _UserService: UserService, private router: Router, private flashMessagesService: FlashMessagesService) {
      router.events.subscribe(event => {
        _UserService.getProfileInfo().subscribe(data => {
          this.log = _UserService.isLogged();
          this.profile = data.user;
          this.loaded = true;
        });
      });
  }
  ngOnInit() {
  }
  logout() {
    this._UserService.logout();
    this.flashMessagesService.show('Saliendo...', {cssClass: 'alert alert-info'});
  }
}
