import { Component, OnInit } from '@angular/core';
import { CompanyService } from '../../../services/company/company.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-closter',
  templateUrl: './closter.component.html',
  styleUrls: ['./closter.component.css']
})
export class ClosterComponent implements OnInit {

  companies: any [] = [];
  constructor(private _companyService: CompanyService, private router: Router) {
    _companyService.getCluster().subscribe(data => {
      this.companies = data.message;
    });
   }

  company(name) {
    this.router.navigate(['company', name]);
  }

  all() {
    this.router.navigate(['closter/all']);
  }

  ngOnInit() {
  }

}
