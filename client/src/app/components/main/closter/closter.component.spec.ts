import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClosterComponent } from './closter.component';

describe('ClosterComponent', () => {
  let component: ClosterComponent;
  let fixture: ComponentFixture<ClosterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClosterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClosterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
