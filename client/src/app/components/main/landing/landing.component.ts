import { Component, OnInit } from '@angular/core';
import { AdminService } from '../../../services/admin/admin.service';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { UserService } from '../../../services/user/user.service';




@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {
  images: any[] = ['1', '2', '3', '4', '5'];
  showEvents: Number;
  closeTab = 1;
  events: any [] = [];
  options: any [] = ['1', '2', '3', '4'];
  active = 0;
  sedes: any [] = [];
  form: FormGroup;
  allies: any [] = [];
  constructor( private adminService: AdminService,
     private formBuilder: FormBuilder, private _userService: UserService) {
    adminService.getEvents().subscribe(data => {
      this.events = data;
    });
    adminService.loadSedes().subscribe(data => {
      this.sedes = data;
    });
    _userService.getAllies().subscribe(data => {
      this.allies = data.message;
    });
    this.createForm();
   }

  ngOnInit() {
  }

  showEvent() {
    this.showEvents = 1;
    this.closeTab = 0;
  }

  close() {
    this.showEvents = 0;
    setTimeout(() => {
      this.closeTab = 1;
    }, 500);
  }

  option(i) {
    this.active = i;
  }

  createForm() {
    this.form = this.formBuilder.group({
      nombre: ['', Validators.required],
      email: ['', Validators.required],
      sede: ['', Validators.required],
      asunto: ['', Validators.required],
      mensaje: ['', Validators.required]
    });
  }
  onSubmit() {
    const mensaje = {
      name : this.form.get('nombre').value,
      email : this.form.get('email').value,
      sede : this.form.get('sede').value,
      bussines : this.form.get('asunto').value,
      text : this.form.get('mensaje').value
    };
    this._userService.sendContact(mensaje).subscribe(data => {
      console.log(data);
    });
  }

}
