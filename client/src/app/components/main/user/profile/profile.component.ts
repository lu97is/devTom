import { Component, OnInit, ElementRef, Input } from '@angular/core';
import { UserService } from '../../../../services/user/user.service';
import {Router} from '@angular/router';
import { Http, Response } from '@angular/http';
import {  FileUploader } from 'ng2-file-upload/ng2-file-upload';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
// change to devel
const URL = 'http://localhost:8080/api/upload';
const URL2 = 'http://localhost:8080/api/uploadBack';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
})
export class ProfileComponent implements OnInit {
  profile: any[] = [];
  companies: any [] = [];
  startups: any [] = [];
  valueAct: number;
  option: number = 0;
  profileLoaded= false;
  companyLoaded = false;
  startupLoaded = false;
  profileInfo: any = {};
  work: boolean;
  study: boolean;
  loaded: boolean;
  socialImg = false;
  addText: String;
  workText = 'Añade Trabajo';
  schoolText = 'Añade Escuela';
  facebookText: String;
  twitterText: String;
  professionText: String;
  instagramText: String;
  bioText: String;  
  interest: any = [];
  habilities: any = [];
  edit = false;
  backgroundImg: String;
  section: Number;
  uploader: FileUploader = new FileUploader({url: URL, itemAlias: 'profileImg', authToken: localStorage.getItem('token')});
  uploaderBack: FileUploader = new FileUploader({url: URL2, itemAlias: 'profileImg', authToken: localStorage.getItem('token')});
  constructor(private _userService: UserService, private router: Router, private el: ElementRef) {
    _userService.getSelfCompanie().subscribe(data => {
      this.companies = data.company;
      this.companyLoaded = true;
    });
    _userService.getSelfStartup().subscribe(data => {
      console.log(data);

      this.startups = data.startup;
      this.startupLoaded = true;
    });
    
    this.load();
   }
   registerComany() {
     this.router.navigate(['company/register']);
   }

   registerStartup() {
    this.router.navigate(['startup/register']);
  }

  load () {
    this._userService.getProfileInfo().subscribe(data => {
      console.log(data);
      
      const fbImg = /.+fbcdn.+/;
      this.profile = data.user;
      this.profileInfo = data.user;
      this.profileLoaded = true;
      this.habilities = this.profileInfo.habilities;
      this.interest = this.profileInfo.interest;
      if(data.user.backUrl == 'default'){
        this.backgroundImg = 'background.jpg' 
      }else{
        this.backgroundImg = data.user.backUrl
      }
      if(!data.user.address){
        this.addText = 'Añade Direccion';
      }else{
        this.addText = this.profileInfo.address
      }
      if(data.user.status){
        if(data.user.status.work){
          this.workText = this.profileInfo.status.work
        }
        if(data.user.status.study){
          this.schoolText = this.profileInfo.status.study
        }
      }
      
      if(!data.user.social.facebook){
        this.facebookText = 'Facebook';
      }else{
        this.facebookText = this.profileInfo.social.facebook
      }
      if(!data.user.social.twitter){
        this.twitterText = 'Twitter';
      }else{
        this.twitterText = this.profileInfo.social.twitter
      }
      if(!data.user.social.instagram){
        this.instagramText = 'Instagram';
      }else{
        this.instagramText = this.profileInfo.social.instagram
      }
      if(!data.user.profession){
        this.professionText = 'Profesion';
      }else{
        this.professionText = this.profileInfo.profession;
      }
      if(!data.user.bio){
        this.bioText = 'Bio';
      }else{
        this.bioText = this.profileInfo.bio;
      }

      if (fbImg.test(this.profileInfo.imageUrl)) {
        this.socialImg = true;
      }
      this.loaded = true;
    });
  }

  ngOnInit() {
    this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false; };
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
         console.log('ImageUpload:uploaded:', item, status, response);
         location.reload();
     };
     this.uploaderBack.onAfterAddingFile = (file2) => { file2.withCredentials = false; };
    this.uploaderBack.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
         console.log('ImageUpload:uploaded:', item, status, response);
         location.reload();
     };
  }


  randomNumber() {
    return Math.floor(Math.random() * 6) + 1;
  }

  company(name) {
    this.router.navigate(['company', name]);
  }

  startup(name) {
    this.router.navigate(['startup', name]);
  }


  value() {
    this.valueAct = 1;
  }


  editUser() {
    this.router.navigate(['profile/edit']);
  }

  // tslint:disable-next-line:member-ordering
  public lineChartData: Array<any> = [
    {data: [this.randomNumber(), this.randomNumber() , this.randomNumber(), this.randomNumber(),
      this.randomNumber(), this.randomNumber(), this.randomNumber()], label: 'Eventos Asistidos'}
  ];
  // tslint:disable-next-line:member-ordering
  public lineChartLabels: Array<any> = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio'];
  // tslint:disable-next-line:member-ordering
  public lineChartOptions: any = {
    responsive: true
  };
  // tslint:disable-next-line:member-ordering
  public lineChartColors: Array<any> = [
    { // grey
      backgroundColor: 'rgba(148,159,255,0.2)',
      borderColor: 'rgb(3, 169, 244)',
      pointBackgroundColor: '#000',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
  // tslint:disable-next-line:member-ordering
  public lineChartLegend = true;
  // tslint:disable-next-line:member-ordering
  public lineChartType = 'line';


  upload() {
        const inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#photo');
        const fileCount: number = inputEl.files.length;
        const formData = new FormData();
        if (fileCount > 0) { formData.append('profileImg', inputEl.files.item(0));
        this._userService.uploadPhoto(formData).subscribe((success) => {
          console.log(success._body);
        }, (error) => {
          console.log(error);
        });
        }
       }

  showCompanies(){
    this.option = 1;
  }
  showStartups(){
    this.option = 2;
  }
  showProfile(){
    this.option = 0;
  }

  editMode(i) {
   if(i == 'editF'){
    this.edit = true;
    this.section = 1;
   }else if(i == 'editS'){
    this.section = 2;
    this.edit = true;
   }else if(i == 'editT'){
    this.section = 3;
    this.edit = true;
   }else if (i == 'editFo'){
    this.edit = true;
    this.section = 4;
   }else {
     this.edit = true;
     this.section = 5;
   }
  }
  closeEdit() {
    this.edit = false;
    this.section = 0;
  }

  save(){
    const user = {
      address : this.addText,
      status: {
        work: this.workText,
        study: this.schoolText
      },
      social: {
        facebook: this.facebookText,
        twitter: this.twitterText,
        instagram: this.instagramText
      },
      profession: this.professionText,
      bio: this.bioText,
      interest: this.interest,
      habilities: this.habilities,
      name: {
        firstName: this.profileInfo.name.firstName
      }
    }
    this._userService.updateProfile(user).subscribe(data => {
      if (data.succes) {
        this.edit = false;
        this.section = 0;
        this.load();
      } else {
        console.error(data)
      }
    })
  }
}
