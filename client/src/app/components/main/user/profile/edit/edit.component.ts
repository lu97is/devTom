import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../../../services/user/user.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  profile: any[] = [];
  form: FormGroup;
  emails: any[] = [];
  email: string;
  used: boolean;
  selected: number;
  validEmail: boolean;
  validPassword: boolean;
  
  study: String;
  work: String;
  interest: any = [];
  habilities: any = [];
  interestSave: any[] = [];
  habilitiesSave: any[] = [];
  positions: any[] = ['Trabajo', 'Estudio', 'Ambos'];
  selectedPosition: number;
  selectedMsg: string;
  messageClass: string;
  message: string;
  loaded = false;
  profileInfo: any = {};
  loading = false;
  updateSucces = false;
  updateMsg: string;
  defaultValues: any = {};
  selectedMsgW: string;
  selectedMsgS: string;
  both = false;

  constructor(private formBuilder: FormBuilder, private _userService: UserService, private router: Router) {
  }

  ngOnInit() {
    this._userService.getProfileInfo().subscribe(data => {
      this.profile = data.user;
      this.profileInfo = data.user;
      this.habilities = this.profileInfo.habilities;
      this.interest = this.profileInfo.interest;
      this.createForm();
      this.loaded = true;
    });
  }
  createForm() {
    if (this.profileInfo.name.firstName === null) {
      this.profileInfo.name.firstName = '';
    }
    if (!this.profileInfo.status) {
      this.work = '';
      this.study = '';
    } else {
      if (this.profileInfo.status.work) {
        this.work = this.profileInfo.status.work;
      }
      if (this.profileInfo.status.study) {
        this.study = this.profileInfo.status.study;
      }
    }
    this.form = this.formBuilder.group({
      email: [this.profileInfo.email],
      firstName: [this.profileInfo.name.firstName, Validators.required],
      lastName: [this.profileInfo.name.lastName, Validators.required],
      work: [this.work],
      study: [this.study],
      phone: [this.profileInfo.phone, Validators.required],
      facebook: [this.profileInfo.social.facebook, Validators.required]
    });
  }

  onChange(newValue) {
    this.used = false;
    // tslint:disable-next-line:max-line-length
    const validEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    this.email = newValue;
    for (let i = 0; i < this.emails.length; i++) {
      if (newValue == this.emails[i]) {
        this.used = true;
      }
    }
    if (validEmail.test(newValue)) {
      this.validEmail = true;
    } else {
      this.validEmail = false;
    }
  }

  onChangePass(newValue) {
    const validPassword =
      /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;
    if (validPassword.test(newValue)) {
      this.validPassword = true;
    } else {
      this.validPassword = false;
    }
  }

  onSumbitReg() {
    // tslint:disable-next-line:forin
    for (let i in this.interest) {
      this.interestSave[i] = this.interest[i];
    }
    // tslint:disable-next-line:forin
    for (let j in this.habilities) {
      this.habilitiesSave[j] = this.habilities[j]
    }
    const user = {
      name: {
        firstName: this.form.controls.firstName.value,
        lastName: this.form.controls.lastName.value,
      },
      study: this.form.get('study').value,
      work: this.form.get('work').value,
      interest: this.interestSave,
      habilities: this.habilitiesSave,
      social: {
        facebook: this.form.get('facebook').value
      },
      phone: this.form.get('phone').value
    };
    this._userService.updateProfile(user).subscribe(data => {
      if (data.succes) {
        this.loading = true;
        this.updateSucces = true;
        this.updateMsg = data.message;
        this.router.navigate(['/profile']);
      } else {
        console.log('aqui algo');
      }
    });
  }

  position(e) {
    this.selectedPosition = e.srcElement.options.selectedIndex;
    if (this.selectedPosition == 0) {
      this.selectedMsg = 'Nombre de la institucion que trabaja';
    } else if (this.selectedPosition == 1) {
      this.selectedMsg = 'Nombre de la institucion donde estudia';
    } else {
      this.both = true;
      this.selectedMsgW = 'Nombre de la institucion que Trabaja';
      this.selectedMsgS = 'Nombre de la institucion que Estudia';
    }
  }

}
