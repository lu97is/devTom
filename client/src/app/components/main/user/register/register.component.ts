import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import {UserService} from '../../../../services/user/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  form: FormGroup;
  form2: FormGroup;
  state = 1;
  message: any;
  messageClass: any;
  emails: any[]= [];
  email: string;
  used: boolean;
  validEmail: boolean;
  validPassword: boolean;
  interest: any []= [];
  habilities: any []= [];
  interestSave: any [] = [];
  habilitiesSave: any [] = [];
  positions: any [] = ['Trabajo', 'Estudio', 'Ambos'];
  selectedPosition: number;
  selectedMsg: string;
  selectedMsgW: string;
  selectedMsgS: string;
  both = false;
  constructor(private formBuilder: FormBuilder, private _regService: UserService, private router: Router) {
    this.createForm();
  }

  ngOnInit() {
    this._regService.getEmails().subscribe(user => {
      for (let i = 0; i < user.length; i++) {
          this.emails[i] = user[i].email;
      }
    });
  }
  createForm() {
    this.form = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required]
    });

    this.form2 = this.formBuilder.group({
      position: ['', Validators.required],
      work: [''],
      study: ['']
    });
  }

  position(e) {
    this.selectedPosition = e.srcElement.options.selectedIndex;
    if (this.selectedPosition == 0) {
        this.selectedMsg = 'Nombre de la institucion que trabaja';
    } else if (this.selectedPosition == 1) {
        this.selectedMsg = 'Nombre de la institucion donde estudia';
    }else {
      this.both = true;
      this.selectedMsgW = 'Nombre de la institucion que Trabaja';
      this.selectedMsgS = 'Nombre de la institucion que Estudia';
    }
  }

  onChange(newValue) {
    this.used = false;
    // tslint:disable-next-line:max-line-length
    const validEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    this.email = newValue;
    for (let i = 0; i < this.emails.length; i++) {
        if (newValue == this.emails[i]) {
            this.used = true;
        }
    }
    if (validEmail.test(newValue)) {
        this.validEmail = true;
    }else {
      this.validEmail = false;
    }

  }

  denied(e) {
    e.preventDefault();
    if (this.form.valid) {
        this.state += 1;
    }
  }
  denied2(e) {
    e.preventDefault();
    if (this.form2.valid) {
        this.state += 1;
    }
  }

  onChangePass(newValue) {
    const validPassword =
    /^(?=(.*[a-zA-Z].*){2,})(?=.*\d.*)(?=.*\W.*)[a-zA-Z0-9_\S]{8,}$/;
    if (validPassword.test(newValue)) {
        this.validPassword = true;
    }else {
      this.validPassword = false;
    }
  }
  next() {
    this.state += 1;
  }
  back() {
    this.state += (-1);
  }
  onSumbitReg() {
    // tslint:disable-next-line:forin
    for (const i in this.interest) {
        this.interestSave[i] = this.interest[i].value;
    }
    // tslint:disable-next-line:forin
    for (const j in this.habilities) {
        this.habilitiesSave[j] = this.habilities[j].value;
    }
    const user = {
      email: this.form.get('email').value,
      password: this.form.get('password').value,
      firstName: this.form.get('firstName').value,
      lastName: this.form.get('lastName').value,
      study: this.form2.get('study').value,
      work: this.form2.get('work').value,
      interest: this.interestSave,
      habilities: this.habilitiesSave
    };
    this._regService.registerUser(user).subscribe(data => {
      if (!data.succes) {
          this.messageClass = 'alert alert-danger';
          this.message = data.message;
      } else {
            this.messageClass = 'alert alert-primary';
            this.message = data.message;
              this._regService.login(user).subscribe(data => {
              this._regService.storeData(data.token, data.user.email);
              this.router.navigate(['profile']);
            });
      }
    });
  }

}
