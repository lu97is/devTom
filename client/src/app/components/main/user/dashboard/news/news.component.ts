import { Component, OnInit } from '@angular/core';
import {AdminService} from '../../../../../services/admin/admin.service';
import {OrderByPipe} from '../../../../../pipes/order-by.pipe';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {
  updates: any [] = [];
  loaded = false;
  constructor(private _adminService: AdminService) {
    _adminService.getUpdates().subscribe(data => {
      this.updates = data;
      this.loaded = true;
    });
  }

  ngOnInit() {
  }

}
