import { Component, OnInit } from '@angular/core';
import {CompanyService} from '../../../../../services/company/company.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-companies',
  templateUrl: './companies.component.html',
  styleUrls: ['./companies.component.css']
})
export class CompaniesComponent implements OnInit {
  companies: any [] = [];
  services: any [] = [];
  loaded: boolean;
  names: any [] = [];
  constructor(private _companyService: CompanyService, private router: Router) {
    _companyService.getCompanies().subscribe(data => {
        this.companies = data.companies;
        this.loaded = true;
        console.log(data);
        
        // tslint:disable-next-line:forin
        for (const i in this.companies) {
            this.names[i] = this.companies[i].services;
        }
      });
  }

  ngOnInit() {
  }

  company(name) {
    this.router.navigate(['company', name]);
  }


}
