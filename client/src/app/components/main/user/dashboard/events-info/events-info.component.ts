import { Component, OnInit } from '@angular/core';
import {AdminService} from '../../../../../services/admin/admin.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-events-info',
  templateUrl: './events-info.component.html',
  styleUrls: ['./events-info.component.css']
})
export class EventsInfoComponent implements OnInit {
  events: any [] = [];
  loaded = false;
  constructor(private _adminService: AdminService, private router: Router) {
    _adminService.getEvents().subscribe(data => {
      this.events = data;
      this.loaded = true;
      console.log(data);
    });
   }

  ngOnInit() {
  }

  information(id) {
    this.router.navigate(['event', id]);
  }

}
