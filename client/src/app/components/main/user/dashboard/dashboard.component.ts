import { Component, OnInit } from '@angular/core';
import {AdminService} from '../../../../services/admin/admin.service';
import {CompanyService} from '../../../../services/company/company.service';
import {OrderByPipe} from '../../../../pipes/order-by.pipe';
import { Router} from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit  {
  events:any [] = []
  updates:any [] = [];
  options:any [] = [];
  companies:any [] = [];
  selected:any = 0;
  constructor(private _adminService:AdminService, private router:Router, private _companyService:CompanyService) {
    _adminService.getEvents().subscribe(data =>{
      this.events = data;
    })
    _adminService.getUpdates().subscribe(data => {
      this.updates = data;
    })

    _companyService.getCompanies().subscribe(data => {
      this.companies = data.companies;
    })

  }

  ngOnInit() {
    this.options.push('Eventos');
    this.options.push('Noticias');
    this.options.push('Empresas')
    this.options.push('Cursos');

  }

  show(i){
    this.selected = i;
  }

  information(id){
    this.router.navigate(['event', id]);
  }

  company(name){
    this.router.navigate(['company', name])
  }


}
