import { Component, OnInit } from '@angular/core';
import {UserService} from '../../../../services/user/user.service';
import {Router} from '@angular/router';
import { AgmCoreModule } from '@agm/core';
import { Renderer2 } from '@angular/core';

@Component({
  selector: 'app-event-info',
  templateUrl: './event-info.component.html',
  styleUrls: ['./event-info.component.css']
})
export class EventInfoComponent implements OnInit {
  event:any [] = [];
  news:any [] = ['1','2','3'];
  lat: number;
  lng: number;
  value:number = 1;
  comments:any [] = ['1','2','3','4']
  number:number;
  street:string;
  city:string;
  active:number = 1;
  loaded:boolean = false;

  constructor(private _userService:UserService, private router:Router, private renderer: Renderer2) {
    let url = this.router.url;
    _userService.getEvent(url).subscribe(data => {
      this.event = data.message;
      this.number = data.message.adress.number;
      this.street = data.message.adress.street;
      this.city = data.message.city;
      this.loaded = true;
      this._userService.getLocation(this.city,this.street,this.number).subscribe(data =>{
        if (data.status != 'ZERO_RESULTS') {
          this.lat = (data.results[0].geometry.location.lat)
          this.lng = (data.results[0].geometry.location.lng)
        }
      })
    })
  }

  ngOnInit() {
  }

  comment(){
    this.renderer.selectRootElement('#commentSelf').focus()
  }

  information(){
    this.value = 1;
  }
  commnets(){
    this.value = 2;
  }

}
