import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../../../../services/user/user.service';
import {Router} from '@angular/router';
import {Location} from '@angular/common';
import { FacebookService, InitParams, LoginResponse, LoginOptions } from 'ngx-facebook';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loading = false;
  messageClass: string;
  message: string;
  validEmail: boolean;
  constructor(private formBuilder: FormBuilder, private _UserService: UserService, private router: Router
    , private fb: FacebookService, private location: Location) {
    const InitParams: InitParams = {
      appId: '350645212059140',
      xfbml: true,
      version: 'v2.8'
    };
    fb.init(InitParams);
    this.createForm();
  }
  form: FormGroup;
  createForm() {
    this.form = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }


  activate() {
    this.form.controls['email'].enable();
    this.form.controls['password'].enable();
  }

  desactivate() {
    this.form.controls['email'].disable();
    this.form.controls['password'].disable();
  }

  ngOnInit() {
  }

  onChange(newValue) {
    // tslint:disable-next-line:max-line-length
    const validEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (validEmail.test(newValue)) {
        this.validEmail = true;
    }else {
      this.validEmail = false;
    }

  }

  login() {
    const user = {
      email: this.form.get('email').value,
      password : this.form.get('password').value
    };
    this.desactivate();
    this._UserService.login(user).subscribe(data => {
      if (!data.succes) {
        this.activate();
        this.messageClass = 'alert alert-danger';
        this.message = data.message;
      }else {
        this.messageClass = 'alert alert-success';
        this.message = data.message;
        this._UserService.storeData(data.token, data.user.email);
        this.router.navigate(['/profile']);
      }
    });
  }

  loginFacebook() {
    const options: LoginOptions = {
      scope: 'public_profile,email,pages_show_list',
      return_scopes: true,
      enable_profile_selector: true
    };
    this.fb.login(options)
      .then((response: LoginResponse) => {
        this.fb.api('/me?fields=id,name,email,picture.type(large)')
          .then((res: any) => {
            const user = {
              email : res.email,
              socialId : res.id,
              imageUrl: res.picture.data.url,
              name: res.name
            };
            this._UserService.registerSocial(user).subscribe(data => {
              this._UserService.socialLogin(user).subscribe(data => {
                this._UserService.storeData(data.token, data.user.email);
                this.router.navigate(['/profile']);
              });
            });
          })
          .catch(e => console.error('err'));
      })
      .catch(e => console.error('Error logging in'));
  }

  linkedin() {
    this._UserService.getLinked().subscribe(data => {
      console.log(data);
    });
  }

}


///auth/linkedin