import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterStartupComponent } from './register-startup.component';

describe('RegisterStartupComponent', () => {
  let component: RegisterStartupComponent;
  let fixture: ComponentFixture<RegisterStartupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterStartupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterStartupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
