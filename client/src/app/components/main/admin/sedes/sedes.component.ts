import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { AdminService} from '../../../../services/admin/admin.service';
import { Router} from '@angular/router';

@Component({
  selector: 'app-sedes',
  templateUrl: './sedes.component.html',
  styleUrls: ['./sedes.component.css']
})
export class SedesComponent implements OnInit {
  form:FormGroup;
  messageClass:any;
  message:any;
  cities:any[] = [];
  constructor(private _adminService:AdminService, private formBuilder:FormBuilder, private router:Router) {
    this.createForm();
  }

  ngOnInit() {
    this._adminService.getCities().subscribe(data =>{
      this.cities = data;
    })
  }
  createForm(){
    this.form = this.formBuilder.group({
      name: ['',Validators.required],
      city: ['', Validators.required]
    })
  }

  onSubmit(){
    const sede = {
      name: this.form.get('name').value,
      city: this.form.get('city').value
    }
    this._adminService.registerSede(sede).subscribe(data => {
      if (!data.succes) {
          this.messageClass = 'alert alert-danger';
          this.message = data.message;
      } else {
            this.messageClass = 'alert alert-success';
            this.message = data.message;
            this.router.navigate(['admin']);
      }
    })
  }
}
