import { Component, OnInit } from '@angular/core';
import { AdminService} from '../../../services/admin/admin.service';
import { CompanyService } from '../../../services/company/company.service';

import { Router} from '@angular/router';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  form: FormGroup;
  formStatus: FormGroup;
  messageClass: any;
  message: any;
  auth: any = false;
  news: number;
  constructor(private _adminService: AdminService, private formBuilder: FormBuilder, private router: Router) {
    this.createForm();

  }

  ngOnInit() {
    if (sessionStorage.getItem('admin')) {
        this.auth = true;
    }else {
      this.auth = false;
    }
  }
  createForm() {
    this.form = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      secret: ['', Validators.required]
    });
    this.formStatus = this.formBuilder.group({
      info: ['', Validators.required],
      priority: ['', Validators.required]
    });
  }

  onSubmit() {
    const admin = {
      username: this.form.get('username').value,
      password: this.form.get('password').value,
      secret: this.form.get('secret').value
    };
    this._adminService.adminLogin(admin).subscribe(data => {
      if (!data.succes) {
          this.messageClass = 'alert alert-danger';
          this.message = data.message;
      } else {
            this.messageClass = 'alert alert-success';
            this.message = data.message;
            sessionStorage.setItem('admin', 'true');
            sessionStorage.setItem('username', data.admin.username);
            sessionStorage.setItem('city', data.admin.city);
            sessionStorage.setItem('sede', data.admin.sede);
            this.auth = true;
      }
    });
  }

  onUpdate(){
    let username = sessionStorage.getItem('username');
    let city = sessionStorage.getItem('city');
    let sede = sessionStorage.getItem('sede');
    let today = new Date();
    let actualDate = today.getDate() + '/' + (today.getMonth() + 1) + '/' + today.getFullYear() + ' ' + today.getHours() + ':' + today.getMinutes();
    const update = {
      info: this.formStatus.get('info').value,
      date: actualDate,
      username: username,
      city: city,
      sede: sede,
      priority: this.formStatus.get('priority').value
    }
      this._adminService.registerUpdate(update).subscribe(data =>{
        if (!data.success) {
            this.messageClass = 'alert alert-danger';
            this.message = data.message;
        } else {
              this.messageClass = 'alert alert-success';
              this.message = data.message;
              this.router.navigate(['admin']);
        }
      })
  }
}
