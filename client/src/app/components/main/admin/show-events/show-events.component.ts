import { Component, OnInit } from '@angular/core';
import {AdminService} from '../../../../services/admin/admin.service';
import { Router} from '@angular/router';

@Component({
  selector: 'app-show-events',
  templateUrl: './show-events.component.html',
  styleUrls: ['./show-events.component.css']
})
export class ShowEventsComponent implements OnInit {
  events:any [] = []
  messageClass:string;
  message:string;
  constructor(private _adminService:AdminService, private router:Router) {
    _adminService.getEvents().subscribe(data =>{
      this.events = data;
    })
  }

  ngOnInit() {
  }
  delete(event){
    this._adminService.deleteEvent(event).subscribe(data => {
      if (!data.success) {
          this.messageClass = 'alert alert-danger';
          this.message = data.message;
      } else {
            this.messageClass = 'alert alert-success';
            this.message = data.message;
            this.router.navigate(['admin']);
      }
    })
  }
}
