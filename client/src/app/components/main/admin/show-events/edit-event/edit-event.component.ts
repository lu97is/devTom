import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { AdminService} from '../../../../../services/admin/admin.service';
import { Router} from '@angular/router';
import { DatepickerOptions } from 'ng2-datepicker'

@Component({
  selector: 'app-edit-event',
  templateUrl: './edit-event.component.html',
  styleUrls: ['./edit-event.component.css']
})
export class EditEventComponent implements OnInit ,  AfterViewInit {
  form:FormGroup;
  messageClass:any;
  message:any;
  cities:any[] = [];
  sedes:any[] = [];
  checked:boolean = false;
  aux:string;
  url:string = this.router.url;
  event:any[] = [];
  model: any = { date: { year: 2018, month: 10, day: 9 } };
    options: DatepickerOptions = {
    minYear: 1970,
    maxYear: 2030,
    displayFormat: 'MMM D[,] YYYY',
    barTitleFormat: 'MMMM YYYY',
    firstCalendarDay: 0
  };
  date:Date
  dateEnd:Date
  constructor(private _adminService:AdminService, private formBuilder:FormBuilder, private router:Router) {
    this._adminService.getEvent(this.url).subscribe(data => {
      this.event = data.message;
    })
    this._adminService.loadSedes().subscribe(data => {
      this.sedes = data;
    })
    this._adminService.getCities().subscribe(data => {
      this.cities = data;
    })
   }

  ngOnInit() {
    this.date = new Date();
    this.dateEnd = new Date();
    this.createForm();
  }

  ngAfterViewInit(){
    this.form.valueChanges.subscribe(data => {
      this.aux = data.city;
    });

  }
  createForm(){
    this.form = this.formBuilder.group({
      name: ['',Validators.required],
      city: ['', Validators.required],
      sede: ['', Validators.required],
      type:['',Validators.required],
      forWho:['',Validators.required],
      price:['',],
      description:['', Validators.required],
      street: ['', Validators.required],
      number: ['', Validators.required],
      email: ['', Validators.required],
      phone: ['', Validators.required]
    })
  }


  onSubmit(){
    let url = this.router.url;
    const event = {
      name: this.form.get('name').value,
      city: this.form.get('city').value,
      sede: this.form.get('sede').value,
      type: this.form.get('type').value,
      forWho: this.form.get('forWho').value,
      myDate: this.date,
      myDateEnd: this.dateEnd,
      price: this.form.get('price').value,
      description: this.form.get('description').value
    }
    this._adminService.editEvent(url,event).subscribe(data => {
      if (!data.success) {
          this.messageClass = 'alert alert-danger';
          this.message = data.message;
      } else {
            this.messageClass = 'alert alert-success';
            this.message = data.message;
            this.router.navigate(['admin']);
      }
    })
  }
}
