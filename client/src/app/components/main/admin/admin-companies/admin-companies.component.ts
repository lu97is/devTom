import { Component, OnInit } from '@angular/core';
import { CompanyService } from '../../../../services/company/company.service';


@Component({
  selector: 'app-admin-companies',
  templateUrl: './admin-companies.component.html',
  styleUrls: ['./admin-companies.component.css']
})
export class AdminCompaniesComponent implements OnInit {
  companies: any [] = [];
  constructor(private companyService: CompanyService) {

    companyService.getCompanies().subscribe(data => {
      this.companies = data.companies;
      console.log(data.companies);
    });
  }

  ngOnInit() {
  }

}
