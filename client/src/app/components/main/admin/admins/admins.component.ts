import { Component, OnInit } from '@angular/core';
import { AdminService} from '../../../../services/admin/admin.service';
import { Router} from '@angular/router';

@Component({
  selector: 'app-admins',
  templateUrl: './admins.component.html',
  styleUrls: ['./admins.component.css']
})
export class AdminsComponent implements OnInit {
  admins:any[] = []
  messageClass:string;
  message:string;
  constructor(private _adminService:AdminService, private router:Router) { }

  ngOnInit() {
    this._adminService.getAdmins().subscribe(data => {
      this.admins = data;
    })
  }
  delete(id){
    this._adminService.deleteAdmin(id).subscribe(data =>{
      if (!data.success) {
          this.messageClass = 'alert alert-danger';
          this.message = data.message;
      } else {
            this.messageClass = 'alert alert-success';
            this.message = data.message;
            this.router.navigate(['admin']);
      }
    })
  }
}
