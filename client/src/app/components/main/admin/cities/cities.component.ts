import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { AdminService} from '../../../../services/admin/admin.service';
import { Router} from '@angular/router';
@Component({
  selector: 'app-cities',
  templateUrl: './cities.component.html',
  styleUrls: ['./cities.component.css']
})
export class CitiesComponent implements OnInit {
  form:FormGroup;
  messageClass:any;
  message:any;
  constructor(private _adminService:AdminService, private formBuilder:FormBuilder, private router:Router) {
    this.createForm();
  }

  ngOnInit() {
  }

  createForm(){
    this.form = this.formBuilder.group({
      name: ['',Validators.required]
    })
  }

  onSubmit(){
    const city = {
      name: this.form.get('name').value
    }
    this._adminService.registerCity(city).subscribe(data => {
      if (!data.succes) {
          this.messageClass = 'alert alert-danger';
          this.message = data.message;
      } else {
            this.messageClass = 'alert alert-success';
            this.message = data.message;
            this.router.navigate(['admin']);
      }
    })
  }
}
