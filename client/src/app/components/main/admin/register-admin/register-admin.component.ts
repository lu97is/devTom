import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router} from '@angular/router';
import { AdminService} from '../../../../services/admin/admin.service';

@Component({
  selector: 'app-register-admin',
  templateUrl: './register-admin.component.html',
  styleUrls: ['./register-admin.component.css']
})
export class RegisterAdminComponent implements OnInit {
  form:FormGroup;
  messageClass:any;
  message:any;
  cities:any [] = [];
  sedes:any[] = [];
  aux:string;
  constructor(private _adminService:AdminService, private formBuilder:FormBuilder, private router:Router) {
    this.createForm();
    this.form.valueChanges.subscribe(data => {
      this.aux = data.city;
    });
  }

  ngOnInit() {
    this._adminService.getCities().subscribe(data => {
      this.cities = data;
    })
    this._adminService.loadSedes().subscribe(data => {
      this.sedes = data;
    })
  }



  createForm(){
    this.form = this.formBuilder.group({
      username: ['',Validators.required],
      password: ['', Validators.required],
      secret: ['', Validators.required],
      city: ['', Validators.required],
      sede:['',Validators.required]
    })
  }
  onSubmit(){
    const admin = {
      username: this.form.get('username').value,
      password: this.form.get('password').value,
      city: this.form.get('city').value,
      secret: this.form.get('secret').value,
      sede: this.form.get('sede').value
    }
    this._adminService.registerAdmin(admin).subscribe(data => {
      if (!data.succes) {
          this.messageClass = 'alert alert-danger';
          this.message = data.message;
      } else {
            this.messageClass = 'alert alert-success';
            this.message = data.message;
            this.router.navigate(['admin']);
      }
    })
  }

}
