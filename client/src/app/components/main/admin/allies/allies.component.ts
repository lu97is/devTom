import { Component, OnInit, ElementRef, Input  } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router} from '@angular/router';
import { AdminService } from '../../../../services/admin/admin.service';
import { Http, Response } from '@angular/http';
import {  FileUploader } from 'ng2-file-upload/ng2-file-upload';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Rx';
const URL = 'http://localhost:8080/api/registerAllie/photo';


@Component({
  selector: 'app-allies',
  templateUrl: './allies.component.html',
  styleUrls: ['./allies.component.css']
})
export class AlliesComponent implements OnInit {
  form: FormGroup;
  allieImg: any;
  validImg = false;
  uploader: FileUploader = new FileUploader({url: URL, itemAlias: 'allieImg'});
  constructor(private adminService: AdminService, private formBuilder: FormBuilder,
    private router: Router, private el: ElementRef) {
    this.createForm();
   }

  ngOnInit() {
    this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false; };
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
         console.log('ImageUpload:uploaded:', item, status, response);
     };
  }

  createForm() {
    this.form = this.formBuilder.group({
      name: ['', Validators.required],
      address: ['', Validators.required],
    });
  }

   public onArchivoSeleccionado($event) {
    if ($event.target.files.length === 1) {
      this.validImg = true;
      this.allieImg = $event.target.files[0];
    }
}
public subirArchivo() {
  const allie = {
    name: this.form.get('name').value,
    address: this.form.get('address').value
  };
  const formData = new FormData();
  formData.append('name', allie.name);
  formData.append('address', allie.address);
  formData.append('allieImg', this.allieImg);
  this.adminService.uploadAlliePhoto(formData).subscribe(data => {
   if (!data.succes) {
     console.log(data);
   } else {
     this.router.navigate(['/admin']);
   }
  });
}

}
