import { Component, OnInit, ElementRef, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {CompanyService} from '../../../../services/company/company.service';
import {UserService} from '../../../../services/user/user.service';
import {  FileUploader } from 'ng2-file-upload/ng2-file-upload';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
// change to devel
const URL = 'http://localhost:8080/api/upload/company';
import { Http, Response } from '@angular/http';


@Component({
  selector: 'app-register-company',
  templateUrl: './register-company.component.html',
  styleUrls: ['./register-company.component.css']
})
export class RegisterCompanyComponent implements OnInit {
  services: any = [];
  tecnologies: any = [];
  serviceSave: any [] = [];
  tecnologiesSave: any [] = [];
  form: FormGroup;
  companyNames: any [] = [];
  used: boolean;
  name: string;
  messageClass: string;
  message: string;
  firstName: string;
  lastName: string;
  id: string;
  rfcType: number;
  validImg: Boolean;
  registerButton = false;
  allieImg: any;
  validEmail: boolean;
  validNumber: boolean;
  validPhone: boolean;
  active = 0;
  progress: any;
  buttons: any [] = ['Cluster', 'Pertenecer'];
  cluster: boolean;
  step = 0;
  uploader: FileUploader = new FileUploader({url: URL, itemAlias: 'profileImg', authToken: localStorage.getItem('token')});
  // tslint:disable-next-line:max-line-length
  constructor(private el: ElementRef, private formBuilder: FormBuilder, private router: Router, private _companyService: CompanyService, private _userService: UserService) {
    this.createForm();
    _userService.getProfileInfo().subscribe(data => {
      this.firstName = data.user.name.firstName;
      this.lastName = data.user.name.lastName;
      this.id = data.user._id;
    });
   }

  selected(i) {
    if (i === 0) {
      this.cluster = true;
    } else {
      this.cluster = false;
    }
  }

  ngOnInit() {
    this._companyService.getCompanies().subscribe(data => {
      // tslint:disable-next-line:forin
      for (const i in data.companies) {
          this.companyNames[i] = data.companies[i].name.toLowerCase();
      }      
    });
  }

  onChange(newValue) {
    this.used = false;
    this.name = newValue;
    for (const i in this.companyNames) {
        if (newValue.toLowerCase() === this.companyNames[i]) {
            this.used = true;
        }
    }
  }
  onChangeNumber(newValue) {
    const validNumber = /^[0-9]{10}$/;
    if (validNumber.test(newValue)) {
        this.validNumber = true;
    } else {
      this.validNumber = false;
    }
  }
  onChangePhone(newValue) {
    const validNumber = /^[0-9]{10}$/;
    if (validNumber.test(newValue)) {
        this.validPhone = true;
    } else {
      this.validPhone = false;
    }
  }
  onChangeRfc(newValue) {
    const fisicRfc = /^[a-zA-Z]{4}\d{6}[a-zA-Z0-9]{3}$/;
    const moralRfc = /^[a-zA-Z]{3}\d{6}[a-zA-Z0-9]{3}$/;
      if (fisicRfc.test(newValue)) {
        this.rfcType = 1;
      } else if (moralRfc.test(newValue)) {
        this.rfcType = 2;
      }else {
        this.rfcType = 0;
      }
  }
  onChangeEmail(newValue) {
    // tslint:disable-next-line:max-line-length
    const validEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (validEmail.test(newValue)) {
        this.validEmail = true;
    }else {
      this.validEmail = false;
    }
  }
  createForm() {
    this.form = this.formBuilder.group({
      name: ['', Validators.required],
      description: ['', Validators.required],
      phrase: ['', Validators.required],
      website: ['', Validators.required],
      approach: ['', Validators.required],
      email: ['', Validators.required, ],
      cellphone: ['', Validators.required],
      phone: ['', Validators.required],
      state: ['', Validators.required],
      street: ['', Validators.required],
      number: ['', Validators.required],
      rfc: ['', Validators.required]
    });
  }

   onArchivoSeleccionado($event) {
    if ($event.target.files.length === 1) {
      this.validImg = true;
      this.allieImg = $event.target.files[0];
    }
    console.log(this.allieImg);
}

  send() {
    // tslint:disable-next-line:forin
    for (const i in this.services) {
        this.serviceSave[i] = this.services[i].value;
    }
    for (const i in this.services) {
      this.tecnologiesSave[i] = this.tecnologies[i].value;
  }
    const Ceo = `${this.firstName} ${this.lastName}`;
    const company = {
      name: this.form.get('name').value,
      description: this.form.get('description').value,
      phrase: this.form.get('phrase').value,
      website: this.form.get('website').value,
      approach: this.form.get('approach').value,
      email: this.form.get('email').value,
      cellphone: this.form.get('cellphone').value,
      phone: this.form.get('phone').value,
      state: this.form.get('state').value,
      street: this.form.get('street').value,
      number: this.form.get('number').value,
      user: Ceo,
      services: this.serviceSave,
      tecnologies: this.serviceSave,
      userId: this.id,
      rfc: this.form.get('rfc').value,
      cluster : this.cluster
    };
    this.active = 1;
    this._companyService.registerCompany(company).subscribe(data => {
      if (!data.succes) {
          this.messageClass = 'alert alert-danger';
          this.message = data.message;
          console.log(data);
      } else {
            this.messageClass = 'alert alert-primary';
            this.message = data.message;
            this.router.navigate(['/profile']);
      }
      console.log(company)
    });
  }

  next() {
    this.step += 1;
    if(this.step == 3){
      setTimeout(() => {
        this.registerButton = true;
      }, 2500);
    }
  }

  back() {
    this.step -= 1;
  }
}
