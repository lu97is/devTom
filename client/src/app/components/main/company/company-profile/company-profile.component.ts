import { Component, OnInit, ElementRef, Input } from '@angular/core';
import { CompanyService } from '../../../../services/company/company.service';
import { StartupService } from '../../../../services/startup/startup.service';

import { Router } from '@angular/router';
import { UserService } from '../../../../services/user/user.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
// change to devel
const URL = 'http://localhost:8080/api/upload';
const URL2 = 'http://localhost:8080/api/uploadBack';

import { Http, Response } from '@angular/http';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';


@Component({
  selector: 'app-company-profile',
  templateUrl: './company-profile.component.html',
  styleUrls: ['./company-profile.component.css'],
})
export class CompanyProfileComponent implements OnInit {
  client: any;
  url: string;
  company: any[] = [];
  startup: any[] = [];
  clients: any[] = [];
  works: any[] = [];
  team: any[] = [];
  option = 0;
  own = false;
  color: any[] = [];
  serachB = false;
  search: any[] = [];
  add: boolean;
  addW: boolean;
  logoPhoto: boolean;
  addC: boolean;
  found: boolean;
  userId: string;
  form: FormGroup;
  socialImg = false;
  types: any[] = ['ai', 'data', 'desing', 'marketing', 'movil', 'web']
  formWork: FormGroup;
  formClient: FormGroup;
  imageUser: String;
  companyUserId: string;
  tabsOptions: any[] = ['Informacion' ,'Clientes', 'Trabajos', 'Equipo'];
  loaded = false;
  image: any;
  name = this.router.url;
  fbImg = /.+fbcdn.+/;
  nameUser: String;
  continue = false;
  companyName: String;
  companyNameS:String;
  backgroundImage: String;
  icons: any = ['fa-info','fa-briefcase', 'fa-folder-open', 'fa-user-circle'];
  uploader: FileUploader = new FileUploader({ url: URL + this.name, itemAlias: 'profileImg', authToken: localStorage.getItem('token') });
  uploaderBack: FileUploader = new FileUploader({ url: URL2 + this.name, itemAlias: 'profileImg', authToken: localStorage.getItem('token') });
  uploaderL: FileUploader = new FileUploader({ url: 'http://localhost:8080/api/clients/' + this.client , itemAlias: 'logoImage', authToken: localStorage.getItem('token') });
  constructor(private formBuilder: FormBuilder, private _companyService: CompanyService,
     private router: Router, private el: ElementRef, private _userService: UserService,
      private _startupService : StartupService) {
    this.url = router.url; 
    _companyService.getCompanie(this.url).subscribe(data => {
      console.log(data)
      if (data.company == null) {
        this.loaded = false;
      } else {
        this.company = data.company;
      if( data.company.backUrl == 'default'){
        this.backgroundImage = 'background.jpg'
      } else{
        this.backgroundImage = data.company.backUrl
      }
      this.companyUserId = data.company.userId;
      this.companyName = data.company._id;
      this.companyNameS = data.company.name;
      this.loaded = true;
      }      
    });
    _userService.getProfileInfo().subscribe(data => {
      if (data.user) {
        this.userId = data.user._id;
      } else {
        this.userId = 'null';
      }
    });
    // _startupService.getStartup()
    this.continue = true;
    if (this.continue = true) {
      setTimeout(() => {
        this.verify();
      }, 200);
    }
  }

  createForm() {
    this.form = this.formBuilder.group({
      email: ['', Validators.required],
      position: ['', Validators.required]
    });
  }

  createFormWork() {
    this.formWork = this.formBuilder.group({
      name: ['', Validators.required],
      desc: ['', Validators.required],
      type: ['', Validators.required],
    });
  }

  type(e) {
    console.log(e.srcElement.options.selectedIndex);
  }

  createFormClient() {
    this.formClient = this.formBuilder.group({
      name: ['', Validators.required],
      webPage: ['', Validators.required],
      desc: ['', Validators.required],
      company: ['', Validators.required],
    });
  }

  optionSelected(i) {
    this.option = i;
    this.loadResources(this.option);
  }

  ngOnInit() {
    this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false; };
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('ImageUpload:uploaded:', item, status, response);
      location.reload();
    };
    this.uploaderBack.onAfterAddingFile = (file2) => { file2.withCredentials = false; };
    this.uploaderBack.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('ImageUpload:uploaded:', item, status, response);
      location.reload();
    };
    // this.uploaderL.onAfterAddingFile = (file) => { file.withCredentials = false; };
    // this.uploaderL.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
    //   console.log('ImageUpload:uploaded:', item, status, response);
    // };
    setTimeout(() => {
      this.optionSelected(0);
    }, 200);

  }

  loadResources(i) {
    if (this.option === 1) {
      this.verify()
     this._companyService.getClients(this.companyNameS).subscribe(data => {
       this.clients = data.message;
     });

    } else if (this.option === 2) {
      this.verify()
      this._companyService.getWorks(this.companyName).subscribe(data => {
        this.works = data.message;
      });
    } else if(this.option === 3){
      this.verify()
      this._companyService.getTeam(this.companyName).subscribe(data => {
        this.team = data.message;
        console.log(this.backgroundImage)
      });
    }
  }
  upload() {
    const inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#photo');
    const fileCount: number = inputEl.files.length;
    const formData = new FormData();
    if (fileCount > 0) {
      formData.append('profileImg', inputEl.files.item(0));
      this._userService.uploadLogo(formData, this.name).subscribe((success) => {
        console.log(success._body);
      }, (error) => {
        console.log(error);
      });
    }
  }

  verify() {
    if (this.userId === this.companyUserId && this.userId != null && this.companyUserId != null) {
      this.own = true;
    }
  }

  addTeam() {
    this.createForm();
    this.add = true;
  }
  closeTeam() {
    this.add = false;
    this.search = [];
  }
  addWork(){
    this.createFormWork();
    this.addW = true;
  }
  closeWork() {
    this.addW = false;
  }
  addClient(){
    this.createFormClient();
    this.addC = true;
  }
  closeClient() {
    this.addC = false;
  }
  searchUser() {
    const user = this.form.get('email').value;
    this._companyService.getProfile(user).subscribe(data => {
      this.serachB = true;
      if (data.message != null) {
        this.found = true;
        this.search = data.message;
        if (this.fbImg.test(data.message.imageUrl)) {
          this.socialImg = true;
        }
        this.imageUser = data.message.imageUrl;
        this.nameUser = data.message.name.firstName + ' ' + data.message.name.lastName;
      }else {
        this.found = false;
      }
    });
  }
  addUser() {
    const user = {
      name: this.nameUser,
      position: this.form.get('position').value,
      img: this.imageUser,
      company: this.companyName
    };
    this._companyService.postTeam(user).subscribe(data => {
      if (data.succes) {
        this.add = false;
        this.loadResources(this.option)
        this.search = [];
      } else {
        console.log(data)
      }
    });
  }

  postWork() {
    const work = {
      name: this.formWork.get('name').value,
      desc: this.formWork.get('desc').value,
      type: this.formWork.get('type').value,
      company: this.companyName
    }
    this._companyService.postWork(work).subscribe(data => {
      if (data.succes) {
        this.addW = false;
        this.loadResources(this.option)
      } else {
        alert(data.message)
      }
    })
    
  }

 

    postClient() {
      const user = {
        name: this.formClient.get('name').value,
        desc: this.formClient.get('desc').value,
        company: this.companyNameS,
        webPage: this.formClient.get('webPage').value
      }
      this._companyService.postClient(user).subscribe(data => {
        if (data.succes) {
          this.addC = false;
          this.loadResources(this.option)
        } else {
          console.log(data)
        }
      })
    }
    onChange(event) {
      this.image = event.srcElement.files;
      console.log(this.image);
    }
}
