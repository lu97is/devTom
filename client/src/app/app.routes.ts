import { RouterModule, Routes } from '@angular/router';
import {LandingComponent} from './components/main/landing/landing.component';
import {LoginComponent} from './components/main/user/login/login.component';
import {RegisterComponent} from './components/main/user/register/register.component';
import {DashboardComponent} from './components/main/user/dashboard/dashboard.component';
import {AdminComponent} from './components/main/admin/admin.component';
import {EventsComponent} from './components/main/admin/events/events.component';
import {CitiesComponent} from './components/main/admin/cities/cities.component';
import {RegisterAdminComponent} from './components/main/admin/register-admin/register-admin.component';
import {AdminsComponent} from './components/main/admin/admins/admins.component';
import {SedesComponent} from './components/main/admin/sedes/sedes.component';
import {AuthGuardService} from './services/auth-guard/auth-guard.service';
import {ShowEventsComponent} from './components/main/admin/show-events/show-events.component';
import {EditEventComponent} from './components/main/admin/show-events/edit-event/edit-event.component';
import {EventInfoComponent} from './components/main/user/event-info/event-info.component';
import {AuthAdminGuardGuard} from './services/auth-guard/auth-admin-guard.guard';
import {RegisterCompanyComponent} from './components/main/company/register-company/register-company.component';
import {ProfileComponent} from './components/main/user/profile/profile.component';
import {CompanyProfileComponent} from './components/main/company/company-profile/company-profile.component';
import {EventsInfoComponent} from './components/main/user/dashboard/events-info/events-info.component';
import {CompaniesComponent} from './components/main/user/dashboard/companies/companies.component';
import {NewsComponent} from './components/main/user/dashboard/news/news.component';
import {AuthLogedService} from './services/auth-guard/auth-loged.service';
import {EditComponent} from './components/main/user/profile/edit/edit.component';
import {ClosterComponent } from './components/main/closter/closter.component';
import {AllComponent} from './components/main/closter/all/all.component';
import {AlliesComponent } from './components/main/admin/allies/allies.component';
import { AdminCompaniesComponent } from './components/main/admin/admin-companies/admin-companies.component';
// import { SocialComponent } from './components/main/social/social.component';
import { StartupComponent } from './components/main/startup/startup.component';
import { RegisterStartupComponent } from './components/main/startup/register-startup/register-startup.component';




const app_routes: Routes = [
  { path: 'home', component: LandingComponent },
  { path: 'login', component: LoginComponent, canActivate: [AuthLogedService]},
  { path: 'register', component: RegisterComponent, canActivate: [AuthLogedService] },
  { path: 'dashboard', component: DashboardComponent, canActivate: [ AuthGuardService ] },
  { path: 'profile', component: ProfileComponent , canActivate: [AuthGuardService]},
  { path: 'profile/edit', component: EditComponent, canActivate: [AuthGuardService]},
  { path: 'event/:id', component: EventInfoComponent },
  { path: 'events', component: EventsInfoComponent },
  { path: 'companies', component: CompaniesComponent },
  { path: 'admin', component: AdminComponent },
  { path: 'news', component: NewsComponent },
  { path: 'admin/cities', component: CitiesComponent, canActivate: [AuthAdminGuardGuard] },
  { path: 'admin/events', component: EventsComponent, canActivate: [AuthAdminGuardGuard] },
  { path: 'admin/register', component: RegisterAdminComponent, canActivate: [AuthAdminGuardGuard] },
  { path: 'admin/admins', component: AdminsComponent , canActivate: [AuthAdminGuardGuard]},
  { path: 'admin/sedes', component: SedesComponent, canActivate: [AuthAdminGuardGuard] },
  { path: 'admin/allies', component: AlliesComponent, canActivate: [AuthAdminGuardGuard] },
  { path: 'admin/event/edit/:id', component: EditEventComponent, canActivate: [AuthAdminGuardGuard] },
  { path: 'admin/coming', component: ShowEventsComponent , canActivate: [AuthAdminGuardGuard]},
  { path: 'admin/companies', component: AdminCompaniesComponent, canActivate: [AuthAdminGuardGuard]},
  { path: 'company/register', component: RegisterCompanyComponent, canActivate: [AuthGuardService]},
  { path: 'startup/register', component: RegisterStartupComponent},
  { path: 'company/:name', component: CompanyProfileComponent },
  { path: 'startup/:name', component: StartupComponent},
  // { path: 'wiwi', component: SocialComponent},
  { path: 'closter', component: ClosterComponent},
  { path: 'closter/all', component: AllComponent},
  { path: '**', pathMatch: 'full', redirectTo: 'home' }
];

export const app_routing = RouterModule.forRoot(app_routes);
