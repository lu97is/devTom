import { Injectable } from '@angular/core';
import {Headers, Http, RequestOptions} from '@angular/http';
import { tokenNotExpired } from 'angular2-jwt';
import { UserService} from '../user/user.service';
import { isDevMode } from '@angular/core';

@Injectable()
export class CompanyService {
    domain2 = 'http://localhost:8080';
    domain;
  constructor(private http: Http, private _userService: UserService) { 
    if (isDevMode()) {
      this.domain = 'http://localhost:8080';
        
      } else {
    //  this.domain = 'http://45.55.133.179:8080';
    this.domain = 'https://devtomato.herokuapp.com';

      }
  }
  registerCompany(company) {
    return this.http.post(this.domain + '/api/registerCompany', company).map(res => res.json());
  }
  getCompanies() {
    return this.http.get(this.domain + '/api/companies').map(res => res.json());
  }
  getCompanie(url) {
    return this.http.get(this.domain + '/api' + url).map(res => res.json());
  }
  getCluster() {
    return this.http.get(this.domain + '/api/companies/cluster').map(res => res.json());
  }
  getTeam(company) {
    return this.http.get(this.domain + '/api/team/' + company, this._userService.options).map(res => res.json());
  }
  getWorks(company) {
    return this.http.get(this.domain + '/api/works/' + company, this._userService.options).map(res => res.json());
  }
  getClients(company) {
    return this.http.get(this.domain + '/api/clients/' + company, this._userService.options).map(res => res.json());
  }
  getLastClient(company) {
    return this.http.get(this.domain + '/api/client/last/' + company, this._userService.options).map(res => res.json());
  }
  postTeam(user) {
    return this.http.post(this.domain + '/api/team', user, this._userService.options).map(res => res.json());
  }
  getProfile(email) {
    return this.http.get(this.domain + '/api/profile/' + email, this._userService.options).map(res => res.json());
  }
  postWork (work) {
    return this.http.post(this.domain + '/api/works', work, this._userService.options).map(res => res.json());
  }
  postClient (client) {
    return this.http.post(this.domain + '/api/clients', client , this._userService.options ).map(res => res.json())
  }

}
