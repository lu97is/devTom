import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate} from '@angular/router';
import {UserService} from '../user/user.service';

@Injectable()
export class AuthLogedService {

  constructor(private userService: UserService, private router: Router) { }
  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.userService.isLogged()) {
        this.router.navigate(['home']);
        return false;

    } else {
      return true;
    }
  }
}
