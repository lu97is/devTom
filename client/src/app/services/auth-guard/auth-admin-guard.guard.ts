import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate} from '@angular/router';


@Injectable()
export class AuthAdminGuardGuard implements CanActivate {
  constructor(private router: Router) { }

  canActivate(next:ActivatedRouteSnapshot, state:RouterStateSnapshot){
    if (sessionStorage.getItem('admin')) {
        return true;
    } else {
      this.router.navigate(['/home'])
      return false;
    }
  }
}
