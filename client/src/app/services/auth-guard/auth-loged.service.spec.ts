import { TestBed, inject } from '@angular/core/testing';

import { AuthLogedService } from './auth-loged.service';

describe('AuthLogedService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthLogedService]
    });
  });

  it('should be created', inject([AuthLogedService], (service: AuthLogedService) => {
    expect(service).toBeTruthy();
  }));
});
