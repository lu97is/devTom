import { Injectable } from '@angular/core';
import {Headers, Http, RequestOptions} from '@angular/http';
import { tokenNotExpired } from 'angular2-jwt';
import { isDevMode } from '@angular/core';

@Injectable()
export class AdminService {
  domain;
  domain2 = 'http://localhost:8080';
  sede;
  authToken;
  admin;
  constructor(private http: Http) { 
    if (isDevMode()) {
      this.domain = 'http://localhost:8080';
        
      } else {
        this.domain = 'https://devtomato.herokuapp.com';

    //  this.domain = 'http://45.55.133.179:8080';
      }
  }
  registerSede(sede) {
    return this.http.post(this.domain + '/api/registerSede', sede).map(res => res.json());
  }
  loadSedes() {
    return this.http.get(this.domain + '/api/loadSedes').map(res => res.json());
  }
  registerAdmin(admin) {
    return this.http.post(this.domain + '/api/adminReg', admin).map(res => res.json());
  }
  getAdmins() {
    return this.http.get(this.domain + '/api/admins').map(res => res.json());
  }
  registerEvent(event) {
    return this.http.post(this.domain + '/api/registerEvent', event).map(res => res.json());
  }
  registerCity(city) {
    return this.http.post(this.domain + '/api/registerCity', city).map(res => res.json());
  }
  getCities() {
    return this.http.get(this.domain + '/api/loadCities').map(res => res.json());
  }
  adminLogin(admin) {
    return this.http.post(this.domain + '/api/adminLog', admin).map(res => res.json());
  }
  storeData(token, admin) {
    localStorage.setItem('token', token);
    localStorage.setItem('admin', JSON.stringify(admin));
    this.authToken = token;
    this.admin = admin;
  }
  isLogin() {
    return tokenNotExpired();
  }
  isAdmin() {
    return this.http.get(this.domain + '/api/role').map(res => res.json());
  }
  getEvents() {
    return this.http.get(this.domain + '/api/events').map(res => res.json());
  }
  registerUpdate(update) {
    return this.http.post(this.domain + '/api/registerUpdate', update).map(res => res.json());
  }
  editEvent(route, event) {
    return this.http.put(this.domain + '/api/' + route, event).map(res => res.json());
  }
  getEvent(event) {
    return this.http.get(this.domain + '/api/' + event ).map(res => res.json());
  }
  deleteEvent(event) {
    return this.http.delete(this.domain + '/api/event/' + event).map(res => res.json());
  }
  deleteAdmin(admin) {
    return this.http.delete(this.domain + '/api/admin/' + admin).map(res => res.json());
  }
  getUpdates() {
    return this.http.get(this.domain + '/api/updates').map(res => res.json());
  }
  registerAllie(allie) {
    return this.http.post(this.domain + '/api/registerAllie', allie).map(res => res.json());
  }
  uploadAlliePhoto(data) {
    return this.http.post(this.domain + '/api/registerAllie/photo' , data).map(res => res.json());
  }
}
