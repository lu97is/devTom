import { Injectable } from '@angular/core';
import {Headers, Http, RequestOptions} from '@angular/http';
import 'rxjs/add/operator/map';
import { tokenNotExpired } from 'angular2-jwt';
import { isDevMode } from '@angular/core';



@Injectable()
export class UserService {
  authToken;
  user;
  options;
  domain;
  domain2 = 'http://localhost:8080'
  key = 'AIzaSyA3qcOnzuXNzWhRC59two-Rh7TXe9Q63J8';
  mapUrl = 'https://maps.googleapis.com/maps/api/geocode/json?address=mexico+';

  constructor(private http: Http) {
    if (isDevMode()) {
    this.domain = 'http://localhost:8080';
      
    } else {
   //this.domain = 'http://45.55.133.179:8080';
     this.domain = 'https://devtomato.herokuapp.com';

    }
   }
  registerUser(user) {
    return this.http.post(this.domain + '/api/register', user).map(res => res.json());
  }
  registerSocial(user) {
    return this.http.post(this.domain + '/api/register/social', user).map(res => res.json());
  }
  socialLogin(user) {
    return this.http.post(this.domain + '/api/socialLogin', user).map(res => res.json());
  }

  sendContact(mes) {
    return this.http.post(this.domain + '/api/contact', mes).map(res => res.json());
  }

  getLocation(city, street, number) {
    return this.http.get(`${this.mapUrl}+${city}+${street}+${number}&key=${this.key}`).map(res => res.json());
  }
  login(user) {
    return this.http.post(this.domain + '/api/login' , user).map(res => res.json());
  }
  storeData(token, user) {
    localStorage.setItem('token', token);
    localStorage.setItem('user', JSON.stringify(user));
    this.authToken = token;
    this.user = user;
  }
  createHeaders() {
    this.loadToken();
    this.options = new RequestOptions({
      headers : new Headers({
        'Content-Type' : 'application/json',
        'authorization' : this.authToken
      })
    });
  }
  loadToken() {
    this.authToken = localStorage.getItem('token');
  }

  getProfile() {
    this.createHeaders();
    return this.http.get(this.domain + '/api/profile', this.options).map(res => res.json());
  }
  getProfileInfo() {
    this.createHeaders();
    return this.http.get(this.domain + '/api/profileInfo', this.options).map(res => res.json());
  }
  getSelfCompanie() {
    this.createHeaders();
    return this.http.get(this.domain + '/api/companies/self', this.options).map(res => res.json());
  }
  getSelfStartup(){
    this.createHeaders();
    return this.http.get(this.domain + '/api/startups/self', this.options).map(res => res.json());
  }
  logout() {
    this.authToken = null;
    this.user = null;
    localStorage.clear();
  }
  isLogin() {
    return tokenNotExpired();
  }
  isLogged() {
    return localStorage.getItem('token');
  }
  getEmails() {
    return this.http.get(this.domain + '/api/emails').map(res => res.json());
  }
  getEvent(id) {
    return this.http.get(this.domain + '/api/' + id).map(res => res.json());
  }
  updateProfile(user) {
    this.createHeaders();
    return this.http.put(this.domain + '/api/register/complete', user, this.options).map(res => res.json());
  }
  uploadPhoto(photo) {
    this.createHeaders();
    return this.http.post(this.domain + '/api/upload', photo , this.options).map(res => res.json());
  }
  uploadLogo(photo, name) {
    this.createHeaders();
    return this.http.post(this.domain + '/api/upload/' + name, photo , this.options).map(res => res.json());
  }
  getAllies() {
    return this.http.get(this.domain + '/api/allies').map(res => res.json());
  }
  getLinked() {
    return this.http.get(this.domain + '/auth/linkedin').map(res => console.log(res));
  }
}
