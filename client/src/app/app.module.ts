import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { app_routing} from './app.routes';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule} from '@angular/http';


//components
import { AppComponent } from './app.component';
import { LandingComponent } from './components/main/landing/landing.component';
import { LoginComponent } from './components/main/user/login/login.component';
import { RegisterComponent } from './components/main/user/register/register.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { DashboardComponent } from './components/main/user/dashboard/dashboard.component';
import { AdminComponent } from './components/main/admin/admin.component';
import { CitiesComponent } from './components/main/admin/cities/cities.component';
import { RegisterAdminComponent } from './components/main/admin/register-admin/register-admin.component';
import { EditEventComponent } from './components/main/admin/show-events/edit-event/edit-event.component';
import { EventInfoComponent } from './components/main/user/event-info/event-info.component';
import { EventsComponent } from './components/main/admin/events/events.component';
import { AdminsComponent } from './components/main/admin/admins/admins.component';
import { SedesComponent } from './components/main/admin/sedes/sedes.component';
import { ShowEventsComponent } from './components/main/admin/show-events/show-events.component';
import {RegisterCompanyComponent} from './components/main/company/register-company/register-company.component';
import { ProfileComponent } from './components/main/user/profile/profile.component';
import { CompanyProfileComponent } from './components/main/company/company-profile/company-profile.component';
import { EventsInfoComponent } from './components/main/user/dashboard/events-info/events-info.component';
import { CompaniesComponent } from './components/main/user/dashboard/companies/companies.component';
import { NewsComponent } from './components/main/user/dashboard/news/news.component';
import { EditComponent } from './components/main/user/profile/edit/edit.component';
import { ClosterComponent } from './components/main/closter/closter.component';
import { AllComponent } from './components/main/closter/all/all.component';
import { AlliesComponent } from './components/main/admin/allies/allies.component';
import { AdminCompaniesComponent } from './components/main/admin/admin-companies/admin-companies.component';
import { StartupComponent } from './components/main/startup/startup.component';
import { RegisterStartupComponent } from './components/main/startup/register-startup/register-startup.component';
// import { SocialComponent } from './components/main/social/social.component';


//outsource
import { TagInputModule } from 'ngx-chips';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AgmCoreModule } from '@agm/core';
import { OrderByPipe } from './pipes/order-by.pipe';
import { FacebookModule } from 'ngx-facebook';
import { ChartsModule } from 'ng2-charts';
import { NgDatepickerModule } from 'ng2-datepicker';
import { FileUploadModule } from 'ng2-file-upload';
import { FlashMessagesModule } from 'angular2-flash-messages';



//services
import { UserService} from './services/user/user.service';
import { AuthGuardService} from './services/auth-guard/auth-guard.service';
import { AuthAdminGuardGuard} from './services/auth-guard/auth-admin-guard.guard';
import { AuthLogedService} from './services/auth-guard/auth-loged.service';
import { AdminService} from './services/admin/admin.service';
import { CompanyService} from './services/company/company.service';
import { StartupService } from './services/startup/startup.service';





@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    LoginComponent,
    RegisterComponent,
    NavbarComponent,
    DashboardComponent,
    AdminComponent,
    CitiesComponent,
    EventsComponent,
    RegisterAdminComponent,
    AdminsComponent,
    SedesComponent,
    ShowEventsComponent,
    EditEventComponent,
    OrderByPipe,
    EventInfoComponent,
    RegisterCompanyComponent,
    ProfileComponent,
    CompanyProfileComponent,
    EventsInfoComponent,
    CompaniesComponent,
    NewsComponent,
    EditComponent,
    ClosterComponent,
    AllComponent,
    AlliesComponent,
    AdminCompaniesComponent,
    StartupComponent,
    RegisterStartupComponent,
    // SocialComponent
  ],
  imports: [
    BrowserModule,
    app_routing,
    FormsModule,
    ReactiveFormsModule,
    ChartsModule,
    HttpModule,
    NgDatepickerModule,
    FlashMessagesModule,
    TagInputModule,
    FileUploadModule,
    FacebookModule.forRoot(),
    BrowserAnimationsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyA3qcOnzuXNzWhRC59two-Rh7TXe9Q63J8'
    })
    ],
  providers: [
    UserService,
    AuthGuardService,
    AdminService,
    AuthAdminGuardGuard,
    CompanyService,
    AuthLogedService,
    StartupService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
