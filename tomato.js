const express = require('express');
const app = express();
const router = express.Router();
const mongoose = require('mongoose');
const config = require('./config/database');
const path = require('path')
const api = require('./routes/api')(router);
const cors = require('cors')
const session = require('express-session');
const bodyParser = require('body-parser');
const port = process.env.PORT || 8080;

mongoose.Promise = global.Promise;
mongoose.connect(config.uri, (err) =>{
  if (err) {
    console.log(err);
  } else {
    console.log("Connected");
  }
});


app.use(cors())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(express.static(__dirname + '/public'))
app.use('/api', api);

app.get('*', function(req, res){
  res.sendFile(path.join(__dirname + '/public/index.html'))
});

app.listen(port, ()=>{
  console.log("app runing on port"+ port);
});
